<?php
/**
 * Created by PhpStorm.
 * User: NVT
 * Date: 12/28/2017
 * Time: 11:56 PM
 */

use \App\Models\Branch;

function listBranch() {
  return Branch::orderBy('id')->get();
}
