<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('bill_number');
          $table->integer('count');
          $table->string('customer_phone');
          $table->string('customer_name');
          $table->string('customer_favorite')->nullable();
          $table->string('customer_address');
          $table->integer('street_id')->unsigned();
          $table->foreign('street_id')->references('id')->on('streets');
          $table->integer('branch_id')->unsigned();
          $table->foreign('branch_id')->references('id')->on('branchs');
          $table->dateTime('kitchen_end');
          $table->dateTime('ship_end');
          $table->integer('shipper_id')->nullable()->unsigned();
          $table->foreign('shipper_id')->references('id')->on('users');
          $table->integer('creator_id')->unsigned();
          $table->foreign('creator_id')->references('id')->on('users');
          $table->integer('ship_price');
          $table->float('distance');
          $table->integer('total_price');
          $table->integer('status');
          $table->text('detail');
          $table->text('reason_cancel')->nullable();
          $table->text('notes')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('bills');
    }
}
