<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distances', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('branch_id')->unsigned();
          $table->foreign('branch_id')->references('id')->on('branchs');
          $table->integer('street_id')->unsigned();
          $table->foreign('street_id')->references('id')->on('streets');
          $table->float('amount');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('distances');
    }
}
