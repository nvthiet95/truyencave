<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StreetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('streets')->insert([
        'name' => 'Trường Chinh',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Tây Sơn',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Láng (Nguyễn Trãi)',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Láng (Yên Lãng)',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Nguyễn Trãi (Trường Chinh)',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Nguyễn Trãi (Khuất Duy Tiến)',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Nguyễn Trãi (Trần Phú)',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Chùa Bộc',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Thái Hà',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Yên Lãng',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Vũ Trọng Phụng',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Nguyễn Tuân',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Thượng Đình',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Khương Đình',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('streets')->insert([
        'name' => 'Hạ Đình',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
