<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('bills')->insert([
        'bill_number' => 1,
        'count' => 1,
        'customer_phone' => '0978456236',
        'customer_name' => 'A Cường',
        'customer_favorite' => 'Thích gà',
        'customer_address' => 'Số 15',
        'notes' => 'Không có ghi chú',
        'street_id' => 14,
        'branch_id' => 1,
        'kitchen_end' => date("Y-m-d") . ' 08:25:00',
        'ship_end' => date("Y-m-d") . ' 08:40:00',
        'creator_id' => 1,
        'ship_price' => 0,
        'total_price' => 120000,
        'distance' => '3.1',
        'status' => 0,
        'detail' => '[{"name":"P1S - B\u00e1nh 1 nh\u1ecf","amount":"2","price":"80000"},{"name":"KM78S - Khuy\u1ebfn m\u00e3i 78 nh\u1ecf","amount":"2","price":"-20000"}]',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('bills')->insert([
        'bill_number' => 2,
        'count' => 1,
        'customer_phone' => '0978451235',
        'customer_name' => 'A Hoàng',
        'customer_favorite' => 'Nhiều tương',
        'customer_address' => 'Số 30 ngõ 112',
        'street_id' => 11,
        'branch_id' => 1,
        'kitchen_end' => date("Y-m-d") . ' 09:25:00',
        'ship_end' => date("Y-m-d") . ' 09:40:00',
        'creator_id' => 1,
        'ship_price' => 30000,
        'total_price' => 120000,
        'distance' => '5.1',
        'status' => 0,
        'detail' => '[{"name":"P1S - B\u00e1nh 1 nh\u1ecf","amount":"2","price":"80000"},{"name":"KM78S - Khuy\u1ebfn m\u00e3i 78 nh\u1ecf","amount":"2","price":"-20000"}]',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('bills')->insert([
        'bill_number' => 3,
        'count' => 1,
        'customer_phone' => '0878564523',
        'customer_name' => 'A Phúc',
        'customer_favorite' => 'Không hành',
        'customer_address' => 'Số 5 ngõ 102 ngách 35',
        'street_id' => 8,
        'branch_id' => 1,
        'kitchen_end' => date("Y-m-d") . ' 09:25:00',
        'ship_end' => date("Y-m-d") . ' 09:40:00',
        'creator_id' => 1,
        'ship_price' => 0,
        'total_price' => 120000,
        'distance' => '2.1',
        'status' => 0,
        'detail' => '[{"name":"P1S - B\u00e1nh 1 nh\u1ecf","amount":"2","price":"80000"},{"name":"KM78S - Khuy\u1ebfn m\u00e3i 78 nh\u1ecf","amount":"2","price":"-20000"}]',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('bills')->insert([
        'bill_number' => 4,
        'count' => 1,
        'customer_phone' => '0948394852',
        'customer_name' => 'A Quang',
        'customer_favorite' => 'Nhiều tương',
        'customer_address' => 'Số 30',
        'notes' => '',
        'street_id' => 12,
        'branch_id' => 2,
        'kitchen_end' => date("Y-m-d") . ' 08:25:00',
        'ship_end' => date("Y-m-d") . ' 08:40:00',
        'creator_id' => 1,
        'ship_price' => 0,
        'total_price' => 120000,
        'distance' => '3.1',
        'status' => 0,
        'detail' => '[{"name":"P1S - B\u00e1nh 1 nh\u1ecf","amount":"2","price":"80000"},{"name":"KM78S - Khuy\u1ebfn m\u00e3i 78 nh\u1ecf","amount":"2","price":"-20000"}]',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('bills')->insert([
        'bill_number' => 5,
        'count' => 1,
        'customer_phone' => '0978451135',
        'customer_name' => 'A Vũ',
        'customer_favorite' => 'Nhiều tương',
        'customer_address' => 'Số 32 ngõ 112',
        'street_id' => 4,
        'branch_id' => 2,
        'kitchen_end' => date("Y-m-d") . ' 09:25:00',
        'ship_end' => date("Y-m-d") . ' 09:40:00',
        'creator_id' => 1,
        'ship_price' => 40000,
        'total_price' => 120000,
        'distance' => '7.1',
        'status' => 0,
        'detail' => '[{"name":"P1S - B\u00e1nh 1 nh\u1ecf","amount":"2","price":"80000"},{"name":"KM78S - Khuy\u1ebfn m\u00e3i 78 nh\u1ecf","amount":"2","price":"-20000"}]',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('bills')->insert([
        'bill_number' => 6,
        'count' => 1,
        'customer_phone' => '0972451135',
        'customer_name' => 'A Đức',
        'customer_address' => 'Nhà D2',
        'street_id' => 6,
        'branch_id' => 2,
        'kitchen_end' => date("Y-m-d") . ' 09:25:00',
        'ship_end' => date("Y-m-d") . ' 09:40:00',
        'creator_id' => 1,
        'ship_price' => 10000,
        'total_price' => 120000,
        'distance' => '7.1',
        'status' => 0,
        'detail' => '[{"name":"P1S - B\u00e1nh 1 nh\u1ecf","amount":"2","price":"80000"},{"name":"KM78S - Khuy\u1ebfn m\u00e3i 78 nh\u1ecf","amount":"2","price":"-20000"}]',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
