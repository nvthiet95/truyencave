<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Nguyễn Văn Thiết',
        'email' => 'thietnguyen@gmail.com',
        'password' => bcrypt('123123'),
        'role' => 'Admin',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('users')->insert([
        'name' => 'Dược BT',
        'email' => 'duocbt@gmail.com',
        'password' => bcrypt('123123'),
        'role' => 'Shipper',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('users')->insert([
        'name' => 'Đại NV',
        'email' => 'dainv@gmail.com',
        'password' => bcrypt('123123'),
        'role' => 'Shipper',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('users')->insert([
        'name' => 'Truyền NV',
        'email' => 'truyennv@gmail.com',
        'password' => bcrypt('123123'),
        'role' => 'Shipper',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('users')->insert([
        'name' => 'Dương HD',
        'email' => 'duonghd@gmail.com',
        'password' => bcrypt('123123'),
        'role' => 'Shipper',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
