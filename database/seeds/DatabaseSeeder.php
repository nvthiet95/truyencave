<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call('UsersTableSeeder');
      $this->command->info('UsersTableSeeder seeded!');
      $this->call('StreetsTableSeeder');
      $this->command->info('StreetsTableSeeder seeded!');
      $this->call('BranchsTableSeeder');
      $this->command->info('BranchsTableSeeder seeded!');
      $this->call('DistancesTableSeeder');
      $this->command->info('DistancesTableSeeder seeded!');
      $this->call('ProductsTableSeeder');
      $this->command->info('ProductsTableSeeder seeded!');
      $this->call('BillsTableSeeder');
      $this->command->info('BillsTableSeeder seeded!');
    }
}
