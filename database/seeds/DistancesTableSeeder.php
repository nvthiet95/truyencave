<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 1,
        'amount' => 1.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 2,
        'amount' => 2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 3,
        'amount' => 1.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 4,
        'amount' => 2.2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 5,
        'amount' => 1.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 6,
        'amount' => 2.7,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 7,
        'amount' => 5.1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 8,
        'amount' => 2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 9,
        'amount' => 2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 10,
        'amount' => 2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 11,
        'amount' => 2.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 12,
        'amount' => 3.1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 13,
        'amount' => 2.2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 14,
        'amount' => 3.1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 1,
        'street_id' => 15,
        'amount' => 2.6,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 1,
        'amount' => 4.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 2,
        'amount' => 5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 3,
        'amount' => 4.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 4,
        'amount' => 5.2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 5,
        'amount' => 4.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 6,
        'amount' => 5.7,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 7,
        'amount' => 8.1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 8,
        'amount' => 5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 9,
        'amount' => 5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 10,
        'amount' => 5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 11,
        'amount' => 5.5,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 12,
        'amount' => 6.1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 13,
        'amount' => 5.2,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 14,
        'amount' => 6.1,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('distances')->insert([
        'branch_id' => 2,
        'street_id' => 15,
        'amount' => 5.6,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
