<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('branchs')->insert([
        'name' => 'Nguyễn Trãi',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('branchs')->insert([
        'name' => 'Ngọc Khánh',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
