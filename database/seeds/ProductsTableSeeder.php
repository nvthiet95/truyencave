<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('products')->insert([
        'name' => 'P1S - Bánh 1 nhỏ',
        'price' => 80000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P2S - Bánh 2 nhỏ',
        'price' => 80000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P3S - Bánh 3 nhỏ',
        'price' => 90000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P4S - Bánh 4 nhỏ',
        'price' => 80000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P5S - Bánh 5 nhỏ',
        'price' => 80000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P1M - Bánh 1 vừa',
        'price' => 100000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P2M - Bánh 2 vừa',
        'price' => 100000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P3M - Bánh 3 vừa',
        'price' => 110000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P4M - Bánh 4 vừa',
        'price' => 100000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P5M - Bánh 5 vừa',
        'price' => 100000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P1L - Bánh 1 lớn',
        'price' => 140000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P2L - Bánh 2 lớn',
        'price' => 140000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P3L - Bánh 3 lớn',
        'price' => 150000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P4L - Bánh 4 lớn',
        'price' => 140000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'P5L - Bánh 5 lớn',
        'price' => 140000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM246L - Khuyến mãi 246 lớn',
        'price' => -80000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM35L - Khuyến mãi 35 lớn',
        'price' => -100000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM78L - Khuyến mãi 78 lớn',
        'price' => -60000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM246M - Khuyến mãi 246 vừa',
        'price' => -40000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM35M - Khuyến mãi 35 vừa',
        'price' => -50000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM78M - Khuyến mãi 78 vừa',
        'price' => -30000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM23456S - Khuyến mãi 23456 nhỏ',
        'price' => -40000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('products')->insert([
        'name' => 'KM78S - Khuyến mãi 78 nhỏ',
        'price' => -20000,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
