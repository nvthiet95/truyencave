### Installation

Install the project dependencies:

1. Setup composer

2. Run command
```
composer install
```

3. Clear cache if it exist
```
php artisan cache:clear
```

4. Generate token key (APP_KEY in .env)
```
php artisan key:generate
```
5. Create database then run command (remember version PHP is 7.1)
```
php artisan migrate:refresh
```
6. if you need dummy data
```
php artisan migrate:refresh --seed
```
or
```
php artisan db:seed
```
