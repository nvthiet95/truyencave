<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
    function post() {
      return $this->belongsTo('App\Post', 'object_id', 'id');
    }
    function game() {
      return $this->belongsTo('App\Game', 'game_id', 'id');
    }
}
