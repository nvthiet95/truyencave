<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';
    function tasks() {
      return $this->hasMany('App\Task', 'game_id', 'id');
    }
}
