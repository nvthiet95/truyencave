<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserTask extends Model
{
  protected $table = 'action_log';
  function post() {
    return $this->belongsTo('App\Post', 'object_id', 'id');
  }
  function game() {
    return $this->belongsTo('App\Game', 'game_id', 'id');
  }
}
