<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGifts extends Model
{
  public function user() {
    return $this->belongsTo('App\User', 'user_id');
  }
  public function gift() {
    return $this->belongsTo('App\Gift', 'gift_id');
  }
}
