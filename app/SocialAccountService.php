<?php
namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\SocialAccount;
use App\User;

class SocialAccountService
{
    public static function createOrGetUser(ProviderUser $providerUser, $social)
    {
        $account = SocialAccount::whereProvider($social)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $email = $providerUser->getEmail() ?? $providerUser->getNickname();
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $social
            ]);
            $user = User::whereEmail($email)->first();

            if (!$user) {
              $displayName = $providerUser->getNickname() ? $providerUser->getNickname() : '';
              $avatar = $providerUser->getAvatar() ? $providerUser->getAvatar() : '';
              $user = User::create([
                  'email' => $email,
                  'name' => $providerUser->getName(),
                  'password' => $providerUser->getName(),
                  'display_name' => $displayName,
                  'avatar' => $avatar,
              ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
