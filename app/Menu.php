<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
  protected function getChildrenMenu($menus, $parentId, $parentSlug) {
    $children = [];
    foreach ($menus as $value) {
      if ($parentId === $value['parents'] && $value['id'] !== 0) {
        array_push($children, $value);
      }
    }
    return $children;
  }

  protected function getMenus() {
    $menus = Menu::orderBy('parents', 'asc')->skip(0)->take(15)->get();
    $mainMenus = [];

    foreach ($menus as $value) {
      if ($value['parents'] === 0 ) {
        $value['children'] = $this->getChildrenMenu($menus, $value['id'], $value['slug']);
        array_push($mainMenus, $value);
      }
    }

    return $mainMenus;
  }
}
