<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  public function menu() {
    return $this->belongsTo('App\Menu', 'menu_id');
  }

  public function user() {
    return $this->belongsTo('App\User', 'user_id');
  }
}
