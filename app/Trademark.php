<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trademark extends Model
{
  public function posts() {
    return $this->hasMany('App\Post')->where('active', 1)->limit(6);
  }
}
