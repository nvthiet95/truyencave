<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

use Validator;
use App\User;
use App\Post;
use App\City;

class UserController extends Controller
{
    protected $module = 'users';
    protected $avatar = 'user_default.jpg';
    protected $cover = 'user_cover.jpg';

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function findUser() {
      $key = request()->has("key") ? request()->get("key") : "";
      $post = User::where("name", "LIKE", "%$key%")->orderBy('created_at', 'desc')->take(10)->get();
      return response()->json($post, 200);
    }
    public function index(Request $request)
    {
        $users = User::orderBy('updated_at', 'desc');
        if ($search = $request->input('search')) {
            $users->where('name', 'like', "%$search%");
        }
        $users = $users->paginate(10);
        foreach ($users as $user) {
          if (!$user->avatar) {
            $user->avatar = $this->avatar;
          }

          // if avatar is absolute link like as google and facebook
          if (!filter_var($user->avatar, FILTER_VALIDATE_URL)) {
            $user->avatar = ENV('LINK_IMAGES') . '/' . $user->avatar;
          }
        }

        return view($this->theme . '.' . $this->module . '.index')
            ->with('users', $users)
            ->with('theme', $this->theme)
            ->with('isUserPage', true);
    }

    public function create()
    {
        $cities = City::orderBy('id', 'desc')->get();
        $inputAvatar = '/' . $this->avatar;
        $inputCover = '/' . $this->cover;
        return view($this->theme . '.' . $this->module . '.create')
            ->with('theme', $this->theme)
            ->with('cities', $cities)
            ->with('inputAvatar', $this->avatar)
            ->with('previewAvatar', ENV('LINK_IMAGES') . $inputAvatar)
            ->with('inputCover', $this->cover)
            ->with('previewCover', ENV('LINK_IMAGES') . $inputCover)
            ->with('isUserPage', true);
    }

    public function store(Request $request)
    {
        try {
            $valid = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email',
                'password' => 'required|string|min:6|confirmed',
            ]);

            if ($valid->passes()) {
                $user = new User();
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->password = bcrypt($request->input('password'));
                $user->display_name = $request->input('display_name');
                $user->cards = $request->input('cards');
                $user->phone = $request->input('phone');
                $user->birthday = $request->input('birthday');
                $user->job = $request->input('job');
                $user->city = $request->input('city');
                $user->website = $request->input('website');
                $user->admin = $request->input('admin');
                $user->points = $request->input('points') ? $request->input('points') : 5;
                if ($request->has('avatar')) {
                    $user->avatar = $request->input('avatar');
                }
                if ($request->has('cover')) {
                    $user->cover = $request->input('cover');
                }
                if ($user->save()) {
                    return response()->json([
                        'message' => trans('lang.COMMON_10'),
                        'user' => $user->toArray()
                    ]);
                }
            } else {
                $errors = $valid->errors();
                $data = [];
                !$errors->has('name') ?: $data['name'] = trans('user.CREATE_ERROR_NAME');
                !$errors->has('email') ?: $data['email'] = trans('user.CREATE_ERROR_EMAIL');
                !$errors->has('password') ?: $data['password'] = trans('user.CREATE_ERROR_PASSWORD');
                return response()->json([
                    'errors' => $data
                ], 500);
            }
        } catch (QueryException $ex) {
            return response()->json([
                'errors' => $ex->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        if ($user = User::find($id)) {
            $cities = City::orderBy('id', 'desc')->get();
            $inputAvatar = '/' . $this->avatar;
            $previewAvatar = ENV('LINK_IMAGES') . $inputAvatar;

            $inputCover = '/' . $this->cover;
            $previewCover = ENV('LINK_IMAGES') . $inputCover;
            if ($user->avatar) {
              $inputAvatar = $user->avatar;
              // if avatar is absolute link like as google and facebook
              if (filter_var($user->avatar, FILTER_VALIDATE_URL)) {
                $previewAvatar = $user->avatar;
              } else {
                $previewAvatar = ENV('LINK_IMAGES') . '/' . $user->avatar;
              }
            }

            if ($user->cover) {
              $inputCover = $user->cover;
              if (filter_var($user->cover, FILTER_VALIDATE_URL)) {
                $previewCover = $user->cover;
              } else {
                $previewCover = ENV('LINK_IMAGES') . '/' . $user->cover;
              }
            }

            return view($this->theme . '.' . $this->module . '.edit')
                ->with('user', $user)
                ->with('theme', $this->theme)
                ->with('cities', $cities)
                ->with('inputAvatar', $inputAvatar)
                ->with('previewAvatar', $previewAvatar)
                ->with('inputCover', $inputCover)
                ->with('previewCover', $previewCover)
                ->with('isUserPage', true);
        }
        return response()->json([
            'errors' => trans('user.ERROR')
        ], 500);
    }

    public function update(Request $request, $id)
    {
        try {
            $valid = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . $id,
            ]);

            if ($valid->passes()) {
                $user = User::find($id);
                if ($user != null) {
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->password = bcrypt($request->input('password'));
                    $user->display_name = $request->input('display_name');
                    $user->cards = $request->input('cards');
                    $user->phone = $request->input('phone');
                    $user->birthday = $request->input('birthday');
                    $user->job = $request->input('job');
                    $user->city = $request->input('city');
                    $user->website = $request->input('website');
                    $user->admin = $request->input('admin');
                    if(!empty($request->input('points'))) $user->points = $request->input('points');
                    if ($request->has('avatar')) {
                        $user->avatar = $request->input('avatar');
                    }
                    if ($request->has('cover')) {
                        $user->cover = $request->input('cover');
                    }
                    if ($user->save()) {
                        return response()->json([
                            'message' => trans('user.UPDATE_SUCCESS'),
                            'user' => $user->toArray()
                        ]);
                    } else {
                        return response()->json([
                            'message' => trans('user.UPDATE_ERROR'),
                            'user' => $user->toArray()
                        ]);
                    }
                } else {
                    return response()->json([
                        'errors' => trans('user.UPDATE_ERROR')
                    ], 500);
                }
            } else {
                $errors = $valid->errors();
                $data = [];
                !$errors->has('name') ?: $data['name'] = trans('user.CREATE_ERROR_NAME');
                !$errors->has('email') ?: $data['email'] = trans('user.CREATE_ERROR_EMAIL');
                return response()->json([
                    'errors' => $data
                ], 500);
            }
        } catch (QueryException $ex) {
            return response()->json([
                'errors' => $ex->getMessage()
            ], 500);
        }
    }


    public function delete(Request $request, $id)
    {
        if ($id == Auth::user()->id) {
            return response()->json([
                'errors' => trans('user.DELETE_ERROR_MYSEFL')
            ], 500);
        } else if ($user = User::find($id)) {
            if ($post = Post::where('user_id', $id)->first()) {
                return response()->json([
                    'errors' => trans('user.DELETE_ERROR_RELATIONSHIP')
                ], 500);
            }
            $user->delete();
            return response()->json([
                'message' => trans('user.DELETE_SUCCESS')
            ]);
        }

        return response()->json([
            'errors' => trans('user.ERROR')
        ], 500);
    }
}
