<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Branch;
use App\Models\Product;
use App\Models\Street;
use Illuminate\Http\Request;
use App\Post;
use App\User;

class HomeController extends Controller
{
  protected $module = 'home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = [];
    $data['streets'] = Street::orderBy('name')->get();
    $data['products'] = Product::orderBy('id')->get();
    $data['shippers'] = User::where('role', 'Shipper')->orderBy('name', 'asc')->get();
    if(!empty(request()->get('branch_id'))) {
      session()->put('branch_id', request()->get('branch_id'));
      return redirect('/');
    } else if(!session()->has('branch_id')) {
      session()->put('branch_id', 1);
      return redirect('/');
    }
    $data['bills'] = Bill::where('branch_id', session()->get('branch_id'))->whereBetween('created_at', [date("Y-m-d"), date("Y-m-d H:i:s", strtotime(date("Y-m-d"))+86399)])
      ->with('street')
      ->with('branch')
      ->with('creator')
      ->with('shipper')
      ->orderBy('id', 'desc')
      ->get();
    return view('pages.home')->with($data);
  }
  public function truyThu()
  {
    $data = [];
    $data['products'] = Product::orderBy('id')->get();
    $data['shippers'] = User::where('role', 'Shipper')->orderBy('name', 'asc')->get();
    $data['total_price'] = 0;
    if(!empty(request()->get('branch_id'))) {
      session()->put('branch_id', request()->get('branch_id'));
      return redirect('/truy-thu');
    } else if(!session()->has('branch_id')) {
      session()->put('branch_id', 1);
      return redirect('/truy-thu');
    }
    $data['bills'] = Bill::where('branch_id', session()->get('branch_id'))
      ->where('status', '!=', -1)
      ->whereBetween('created_at', [date("Y-m-d"), date("Y-m-d H:i:s", strtotime(date("Y-m-d"))+86399)])
      ->with('street')
      ->with('branch')
      ->with('creator')
      ->with('shipper')
      ->orderBy('id', 'desc')
      ->get();
    $group_shipper = [];
    foreach ($data['bills'] as $v) {
      if(!empty($v->shipper_id)) {
        if(empty($group_shipper[$v->shipper_id])) {
          $group_shipper[$v->shipper_id] = [
            'bills' => [],
            'price' => 0,
            'distance' => 0
          ];
        }
        $group_shipper[$v->shipper_id]['bills'][] = $v;
        $group_shipper[$v->shipper_id]['price'] += $v->total_price;
        $group_shipper[$v->shipper_id]['distance'] += $v->distance;
        $data['total_price'] += $v->total_price;
      }
    }
    $data['group_shipper'] = $group_shipper;
    return view('pages.truy-thu')->with($data);
  }
}
