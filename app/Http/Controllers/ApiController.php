<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Distance;
use App\Models\Street;
use Illuminate\Http\Request;
use App\Post;
use App\User;

class ApiController extends Controller
{
  protected $module = 'home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function getDistance()
  {
    $street_id = request()->has('street_id') ? request()->get('street_id') : 0;
    $distances = Distance::where('street_id', $street_id)->orderBy('amount', 'asc')->with('branch')->get();
    return response()->json($distances, 200);
  }
  public function postCreateBill()
  {
    $maxBill = Bill::orderBy('id', 'desc')->first();
    $bill = new Bill();
    $bill->bill_number = !empty($maxBill) ? ($maxBill->bill_number + 1) : 1;
    $bill->customer_phone = request()->get('customer_phone');
    $bill->customer_address = request()->get('customer_address');
    $bill->count = Bill::where('customer_phone', request()->get('customer_phone'))->count() + 1;
    $bill->customer_name = request()->get('customer_name');
    $bill->street_id = request()->get('streets');
    $bill->customer_favorite = request()->get('customer_favorite');
    $bill->branch_id = request()->get('branch');
    $bill->kitchen_end = $this->formatDate(request()->get('kitchen_date')) . ' ' . request()->get('kitchen_time');
    $bill->ship_end = $this->formatDate(request()->get('ship_date')) . ' ' . request()->get('ship_time');
    $bill->creator_id = auth()->user()->id;
    $bill->ship_price = request()->get('ship_money');
    $bill->distance = request()->get('distance');
    $bill->total_price = request()->get('total_money');
    $bill->status = 0;
    $bill->detail = json_encode(request()->get('products'));
    $bill->save();
    return response()->json(request()->all(), 200);
  }
  function formatDate($date) {
    $chia = explode('/', $date);
    $tmp = $chia[0];
    $chia[0] = $chia[2];
    $chia[2] = $tmp;
    return implode('/', $chia);
  }
  function getCheckCustomer() {
    $bill =Bill::where('customer_phone', request()->get('customer_phone'))->orderBy('id', 'desc')->first();
    if(!empty($bill)) return response()->json($bill, 200);
    else return response()->json(['message' => 'Không tìm thấy'], 404);
  }
  function postUpdateBill() {
    $bill_id = !empty(request()->get('id')) ? request()->get('id') : 0;
    $thisBill = Bill::find($bill_id);
    if(!empty($thisBill)) {
      if(!empty(request()->get('status')) && (request()->get('status') == 1 || request()->get('status') == 2)) {
        $thisBill->status = request()->get('status');
        $thisBill->save();
      }
      if(!empty(request()->get('shipper'))) {
        $thisBill->shipper_id = request()->get('shipper');
        $thisBill->save();
      }
      if(request()->get('gio') != NULL && request()->get('gio') != '' && request()->get('phut') != NULL && request()->get('phut') != '') {
        $thisBill->kitchen_end = date("Y-m-d", strtotime($thisBill->kitchen_end)).' '.request()->get('gio') . ':'.request()->get('phut');
        $thisBill->save();
      }
      if(!empty(request()->get('reason_cancel'))) {
        $thisBill->status = -1;
        $thisBill->reason_cancel = request()->get('reason_cancel');
        $thisBill->save();
        return response()->json(['message' => 'Hủy đơn hàng thành công'], 200);
      }
    }
    return redirect()->back();
  }
}
