<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Distance;
use App\Models\Street;
use Illuminate\Http\Request;
use App\Post;
use App\User;

class ScreenController extends Controller
{
  protected $module = 'home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  function bep() {
    $data = [];
    if(!empty(request()->get('branch_id'))) {
      session()->put('branch_id', request()->get('branch_id'));
      return redirect('/bep');
    } else if(!session()->has('branch_id')) {
      session()->put('branch_id', 1);
      return redirect('/bep');
    }
    $data['bills'] = Bill::where('status', '!=', '-1')->whereBetween('ship_end', [date("Y-m-d"), date("Y-m-d H:i:s", strtotime(date("Y-m-d"))+86399)])->where('branch_id', session()->get('branch_id'))->where('status', '<', 2)->with('street')->orderBy('kitchen_end')->get();
    return view('pages.bep')->with($data);
  }
  function tongDai() {
    $data = [];
    if(!empty(request()->get('branch_id'))) {
      session()->put('branch_id', request()->get('branch_id'));
      return redirect('/tong-dai');
    } else if(!session()->has('branch_id')) {
      session()->put('branch_id', 1);
      return redirect('/tong-dai');
    }
    $data['bills'] = Bill::where('status', '!=', '-1')->whereBetween('ship_end', [date("Y-m-d"), date("Y-m-d H:i:s", strtotime(date("Y-m-d"))+86399)])->where('branch_id', session()->get('branch_id'))->where(function ($query){
      $query->where('status', '<', 2)->orWhereNull('shipper_id');
    })->orderBy('kitchen_end')->with('street')->get();
    $data['shippers'] = User::where('role', 'Shipper')->orderBy('name', 'asc')->get();
    return view('pages.tong-dai')->with($data);
  }
}
