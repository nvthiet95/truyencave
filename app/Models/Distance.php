<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distance extends Model
{
    protected $table = 'distances';
    public function branch() {
      return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
    }
}
