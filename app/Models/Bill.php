<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
  protected $table = 'bills';

  public function street()
  {
    return $this->belongsTo('App\Models\Street', 'street_id', 'id');
  }
  public function branch()
  {
    return $this->belongsTo('App\Models\Branch', 'branch_id', 'id');
  }
  public function creator()
  {
    return $this->belongsTo('App\User', 'creator_id', 'id');
  }
  public function shipper()
  {
    return $this->belongsTo('App\User', 'shipper_id', 'id');
  }
}
