<?php
    return [
        'CONFIRM_REMOVE' => 'Are you sure to delete this row ?',
        'ERROR' => 'Error !',
        'CREATE_SUCCESS' => 'Create post success !',
        'CREATE_ERROR' => 'Create post error !',
        'CREATE_ERROR_NAME' => 'Please check your name',
        'DELETE_SUCCESS' => 'Delete post success',
        'UPDATE_SUCCESS' => 'Update post success',
        'UPDATE_ERROR' => 'Update post fail'
    ];
?>