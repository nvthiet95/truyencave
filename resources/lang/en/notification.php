<?php
    return [
        'NOTIFICATION' => 'Notifications',
        'ALL_NOTI' => 'All notifications',
        'ADD_NOTI' => 'Add new',
        'NOTI_ID' => 'ID',
        'NOTI_DESCRIPTION' => 'Description',
        'NOTI_USER_RECIVE_NAME' => 'User recive',
        'NOTI_USER_READED' => 'Readed',
        'NOTI_TIME' => 'Time',
        'EDIT' => 'Edit',
        'REMOVE' => 'Remove',
        'CREATE' => 'Create Notification',
        'EDIT' => 'Edit Notification',
        'CONFIRM_REMOVE' => 'Are you sure to delete this row ?',
        'ERROR' => 'Error !',
        'CREATE_SUCCESS' => 'Create notification success !',
        'CREATE_ERROR' => 'Create notification error !'
    ];
?>