<?php
    return [
        'SLIDE' => 'Slide',
        'ID' => 'ID',
        'ALL_SLIDE' => 'All Slide',
        'ADD_SLIDE' => 'Add new',
        'SLIDE_TITLE' => 'Title',
        'SLIDE_LINK' => 'Link',
        'SLIDE_DESCRIPTION' => 'Description',
        'SLIDE_IMAGE' => 'Image',
        'CONFIRM_REMOVE' => 'Are you sure to delete this row ?',
        'ERROR' => 'Error !',
        'CREATE_SUCCESS' => 'Create slide success !',
        'CREATE_ERROR' => 'Create slide error !',
        'CREATE_ERROR_TITLE' => 'Please check your title',
        'UPDATE_ERROR_TITLE' => 'Please check your title',
        'DELETE_SUCCESS' => 'Delete user success',
        'UPDATE_SUCCESS' => 'Update user success',
        'UPDATE_ERROR' => 'Update user fail'
    ];
?>