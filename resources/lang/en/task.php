<?php
return [
  'NAME' => 'Task',
  'ADD' => 'Add new',
  'ALL' => 'Task manager',
  'CREATE_ADD' => 'All task',
  'GAME_TITLE' => 'Game title',
  'TYPE' => 'Type',
  'OBJECT' => 'Object',
  'POINT' => 'Point',
  'CREATE_FORM_CREATE' => 'Form create',
  'CREATE_TASKS' => 'List tasks',
  'CREATE_SELECT_TASKS' => 'Select tasks',
  'CREATE_FINISHED_AT' => 'Finished at',
  'CREATE_SUCCESS' => 'Create task success',
  'UPDATE_SUCCESS' => 'Update task success',
  'CREATE_ERROR' => 'Create task error',
  'ID' => 'ID',
  'DELETE_SUCCESS' => 'Delete task success',
  'DELETE_ERROR' => 'Delete task error',
  'CONFIRM_REMOVE' => 'Are you sure to delete this task ?',
  'GAME_NOT_FOUND' => 'Game not found ?',
  'all_desc' => 'Xem, thích, chia sẻ những bài viết dưới đây để hoàn thành nhiệm vụ',
  'like_desc' => 'Danh sách bài viết cần thích',
  'view_desc' => 'Danh sách bài viết cần xem',
  'share_desc' => 'Danh sách bài viết cần chia sẻ',
  'like_name' => 'THÍCH BÀI VIẾT',
  'view_name' => 'XEM BÀI VIẾT',
  'share_name' => 'CHIA SẺ BÀI VIẾT',
  'create_post_name' => 'TẠO BÀI VIẾT',
];
?>