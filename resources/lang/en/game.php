<?php
return [
  'LEFTBAR_GAME' => 'Game',
  'LEFTBAR_ADD' => 'Add new',
  'LEFTBAR_ALL' => 'All game',
  'CREATE_ADD' => 'All game',
  'CREATE_FORM_CREATE' => 'Form create',
  'CREATE_TITLE' => 'Title',
  'CREATE_DESC' => 'Description',
  'CREATE_POINT' => 'Point',
  'CREATE_TASKS' => 'List tasks',
  'CREATE_SELECT_TASKS' => 'Select tasks',
  'CREATE_FINISHED_AT' => 'Finished at',
  'CREATE_SUCCESS' => 'Create game success',
  'UPDATE_SUCCESS' => 'Update game success',
  'CREATE_ERROR' => 'Create game error',
  'ID' => 'ID',
  'DELETE_SUCCESS' => 'Delete game success',
  'DELETE_ERROR' => 'Delete game error',
  'CONFIRM_REMOVE' => 'Are you sure to delete this game ?',
  'GAME_NOT_FOUND' => 'Game not found ?',
  'GAME_STATUS' => 'Status'
];
?>