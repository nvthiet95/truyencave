<?php
    return [
        'CONFIRM_REMOVE' => 'Are you sure to delete this row ?',
        'ERROR' => 'Error !',
        'CREATE_SUCCESS' => 'Create user success !',
        'CREATE_ERROR' => 'Create user error !',
        'CREATE_ERROR_NAME' => 'Please check your name',
        'CREATE_ERROR_EMAIL' => 'Please check your email',
        'CREATE_ERROR_PASSWORD' => 'Please check your password',
        'DELETE_ERROR_RELATIONSHIP' => 'Cannot delete this user',
        'DELETE_ERROR_MYSEFL' => 'Can not delete myself',
        'DELETE_SUCCESS' => 'Delete user success',
        'UPDATE_SUCCESS' => 'Update user success',
        'UPDATE_ERROR' => 'Update user fail',
        'UPDATE_USER' => 'Update user info'
    ];
?>