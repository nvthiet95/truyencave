<!DOCTYPE html>
<html>
  <head>
    @include('includes.head')
    @yield('page_styles')
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    @include('includes.header')
    @include('includes.sidebar')
    @yield('content')
    @include('includes.footer')
  </div>
  @include('includes.javascript')
  @yield('page_scripts')
  </body>
</html>