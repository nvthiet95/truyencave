<!DOCTYPE html>
<html>
  <head>
    @include('includes.head')
    @yield('page_styles')
  </head>
  <body class="hold-transition login-page">
    @yield('content')
  </body>
  @include('includes.javascript')
  @yield('page_scripts')
</html>