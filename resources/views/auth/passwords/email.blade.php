@extends('layouts.login')
@section('page_styles')
<link href="{{ asset('default/css/pages/login.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <form class="form-vertical forget-form" role="form" method="POST" action="{{ url('/password/email') }}">
      {{ csrf_field() }}
      <h3 class="">{{ trans('lang.USER_12') }}</h3>
      <p>{{ trans('lang.USER_13') }}</p>
      @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
      @endif
      <div class="control-group{{ $errors->has('email') ? ' error' : '' }}">
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-envelope"></i>
            <input class="m-wrap placeholder-no-fix" type="email" placeholder="{{ trans('lang.USER_EMAIL') }}" name="email" value="{{ old('email') }}" />
          </div>
          @if ($errors->has('email'))
            <label for="email" class="help-inline help-small no-left-padding">{{ $errors->first('email') }}</label>
          @endif
        </div>
      </div>
      <div class="form-actions">
        <a href="{{ url('/login') }}" id="back-btn" class="btn">
            <i class="m-icon-swapleft"></i> {{ trans('lang.BUTTON_1') }}
        </a>
        <button type="submit" class="btn green pull-right">
            {{ trans('lang.BUTTON_2') }} <i class="m-icon-swapright m-icon-white"></i>
        </button>
      </div>
    </form>
@endsection
