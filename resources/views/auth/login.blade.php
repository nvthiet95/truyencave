@extends('layouts.login')
@section('title', 'Đăng nhập Pizza Manager')
@section('content')
  <div class="login-box">
    <div class="login-logo">
      <a href="/login"><b>Pizza</b>Manager</a>
    </div>
    <div class="login-box-body">
      <p class="login-box-msg">Đăng nhập vào Pizza</p>

      <form action="{{ url('/login') }}" id="login" method="post">
        {{ csrf_field() }}
        @if (session('messageRegSuccess'))
          <div class="alert alert-success">
            {{ session('messageRegSuccess') }}
          </div>
        @endif
        <div class="alert alert-error {{ ($errors->has('email') || $errors->has('password')) ? '' : 'hide' }}">
          <button class="close" data-dismiss="alert"></button>
          @if ($errors->has('email'))
            <span>
                <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
          @if ($errors->has('password'))
            <span>
                <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group has-feedback">
          <input type="email" class="form-control" name="email" placeholder="Email">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <button type="submit"
                    class="btn btn-primary btn-block btn-flat"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Đang xử lý..."
            >Đăng Nhập</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop
@section('page_scripts')
  <script>
    $(document).ready(function () {
      $('#login').submit(function () {
        $('#login button').button('loading');
      });
    });
  </script>
@stop
