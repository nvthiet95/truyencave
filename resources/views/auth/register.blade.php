@extends('layouts.login')
@section('page_styles')
<link href="{{ asset('default/css/pages/login.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="form-vertical register-form" role="form" method="POST" action="{{ url('/register') }}">
      {{ csrf_field() }}
      <h3>{{ trans('lang.USER_15') }}</h3>
      <p>{{ trans('lang.USER_16') }}</p>
      <div class="control-group{{ $errors->has('name') ? ' error' : '' }}">
        <label class="control-label visible-ie8 visible-ie9">{{ trans('lang.USER_2') }}</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-user"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="{{ trans('lang.USER_NAME') }}" name="name" value="{{ old('name') }}"/>
          </div>
        </div>
        @if ($errors->has('name'))
        <label class="help-inline help-small no-left-padding">{{ $errors->first('name') }}</label>
        @endif
      </div>
      <div class="control-group{{ $errors->has('email') ? ' error' : '' }}">
        <label class="control-label visible-ie8 visible-ie9">{{ trans('lang.USER_EMAIL') }}</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-envelope"></i>
            <input class="m-wrap placeholder-no-fix" type="email" placeholder="{{ trans('lang.USER_EMAIL') }}" name="email" value="{{ old('email') }}"/>
          </div>
        </div>
        @if ($errors->has('email'))
        <label class="help-inline help-small no-left-padding">{{ $errors->first('email') }}</label>
        @endif
      </div>
      <div class="control-group{{ $errors->has('password') ? ' error' : '' }}">
        <label class="control-label visible-ie8 visible-ie9">{{ trans('lang.USER_PASSWORD') }}</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-lock"></i>
            <input class="m-wrap placeholder-no-fix" type="password" id="register_password" placeholder="{{ trans('lang.USER_PASSWORD') }}" name="password" value="{{ old('password') }}"/>
          </div>
        </div>
        @if ($errors->has('password'))
        <label class="help-inline help-small no-left-padding">{{ $errors->first('password') }}</label>
        @endif
      </div>
      <div class="control-group{{ $errors->has('password_confirmation') ? ' error' : '' }}">
        <label class="control-label visible-ie8 visible-ie9">{{ trans('lang.USER_PASSWORD_CONFIRM') }}</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-ok"></i>
            <input class="m-wrap placeholder-no-fix" type="password" placeholder="{{ trans('lang.USER_PASSWORD_CONFIRM') }}" name="password_confirmation"/>
          </div>
        </div>
        @if ($errors->has('password_confirmation'))
        <label class="help-inline help-small no-left-padding">{{ $errors->first('password_confirmation') }}</label>
        @endif
      </div>
      <div class="form-actions">
        <a href="{{ url('/login') }}" type="button" class="btn">
            <i class="m-icon-swapleft"></i>  {{ trans('lang.BUTTON_1') }}
        </a>
        <button type="submit" id="register-submit-btn" class="btn green pull-right">
            {{ trans('lang.BUTTON_3') }} <i class="m-icon-swapright m-icon-white"></i>
        </button>
      </div>
    </form>
    <!-- END REGISTRATION FORM -->
@stop
