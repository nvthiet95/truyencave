<div class="modal fade" id="create-bill">
  <div class="modal-dialog popup-create-bill">
    <div class="modal-content">
      <form action="" id="submit-create-bill">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><strong>TẠO ĐƠN HÀNG</strong></h4>
        </div>
        <div class="modal-body">
          <div class="customer-info">
            <div class="title-customer-info">
              Thông tin khách hàng
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    <input name="customer_phone" required type="text" class="form-control" placeholder="Điện thoại">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-user"></i>
                    </div>
                    <input name="customer_name" required type="text" class="form-control" placeholder="Tên khách hàng">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-home"></i>
                    </div>
                    <input name="customer_address" required type="text" class="form-control" placeholder="Số nhà">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-map"></i>
                    </div>
                    <select name="streets" required style="width: 100%" class="form-control">
                      <option value=""></option>
                      @if(!empty($streets))
                        @foreach($streets as $v)
                          <option value="{{ $v->id }}">{{ $v->name }}</option>
                        @endforeach
                      @endif
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon icon-branch">
                      <i class="fa fa-map-marker"></i>
                    </div>
                    <select name="branch" required style="width: 100%" class="form-control"></select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-check-circle"></i>
                    </div>
                    <input name="customer_favorite" type="text" class="form-control" placeholder="Sở thích">
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Bếp
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" required name="kitchen_date" value="{{ date("d/m/Y") }}" class="form-control" placeholder="Thời hạn" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <select required name="kitchen_time" style="width: 100%" class="form-control">
                      @for($i=0; $i<24; $i++)
                        <option value=""></option>
                        @for($j=0; $j<60; $j=$j+5)
                          <option value="{{ ($i < 10 ? '0'.$i : $i).':'.($j < 10 ? '0'.$j : $j) }}">{{ ($i < 10 ? '0'.$i : $i).':'.($j < 10 ? '0'.$j : $j) }}</option>
                        @endfor
                      @endfor
                    </select>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Nhân viên vận chuyển
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-car"></i>
                    </div>
                    <input type="text" required name="distance" readonly class="form-control" placeholder="Km">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-dollar"></i>
                    </div>
                    <input name="ship_money" required type="text" class="form-control" placeholder="Phí vận chuyển">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" required name="ship_date" value="{{ date("d/m/Y") }}" class="form-control" placeholder="Thời hạn">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <select required name="ship_time" style="width: 100%" class="form-control">
                      @for($i=0; $i<24; $i++)
                        <option value=""></option>
                        @for($j=0; $j<60; $j=$j+5)
                          <option value="{{ ($i < 10 ? '0'.$i : $i).':'.($j < 10 ? '0'.$j : $j) }}">{{ ($i < 10 ? '0'.$i : $i).':'.($j < 10 ? '0'.$j : $j) }}</option>
                        @endfor
                      @endfor
                    </select>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Đặt món
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <select style="width: 100%" class="form-control product">
                    <option value=""></option>
                    @if(!empty($products))
                      @foreach($products as $v)
                        <option value="{{ $v->id }}_{{ $v->price }}">{{ $v->name }}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="col-md-3">
                  <input type="number" value="1" class="form-control amount" placeholder="Số lượng" />
                </div>
                <div class="col-md-3" class="text-center"><button id="add_product" type="button" class="btn btn-success form-control">Thêm</button></div>
                <div class="clearfix"></div>
                <br />
              </div>
              <table class="table table-bordered">
                <thead>
                <tr>
                  <th style="width: 40%">Tên sản phẩm</th>
                  <th style="width: 20%">Số lượng</th>
                  <th style="width: 25%">Giá</th>
                  <th style="width: 15%">Hành động</th>
                </tr>
                </thead>
                <tbody class="list_product"></tr>
                </tbody>
              </table>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Ghi chú
            </div>
            <div class="detail-customer-info">
              <textarea name="notes" class="note-bill form-control " rows="3"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-left" style="color: red; font-size: 20px; line-height: 34px;"><strong>Tổng tiền:</strong> <span id="total_money">0</span>đ</div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="submit" class="submit-create-bill btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Đang xử lý ...">Lưu</button>
        </div>
      </form>
    </div>
  </div>
</div>