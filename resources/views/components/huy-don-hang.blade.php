<div class="modal fade" id="cancel-bill">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" id="submit-cancel-bill">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><strong>HỦY ĐƠN HÀNG</strong></h4>
        </div>
        <div class="modal-body">
          <input type="hidden" required id="bill_cancel_id" />
          <textarea rows="3" required class="reason-cancel-bill form-control" placeholder="Lý do hủy đơn"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="submit" class="submit-cancel-bill btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Đang xử lý ...">Xác nhận</button>
        </div>
      </form>
    </div>
  </div>
</div>