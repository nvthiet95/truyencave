<div class="modal fade" id="show-bill">
  <div class="modal-dialog popup-create-bill">
    <div class="modal-content">
      <form action="" id="submit-create-bill">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><strong>XEM LẠI ĐƠN HÀNG</strong></h4>
        </div>
        <div class="modal-body">
          <div class="customer-info">
            <div class="title-customer-info">
              Thông tin khách hàng
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-phone"></i>
                    </div>
                    <input readonly type="text" class="customer_phone form-control" placeholder="Điện thoại">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-user"></i>
                    </div>
                    <input readonly type="text" class="customer_name form-control" placeholder="Tên khách hàng">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-home"></i>
                    </div>
                    <input readonly type="text" class="customer_address form-control" placeholder="Số nhà">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-map"></i>
                    </div>
                    <input readonly style="width: 100%" class="streets form-control"></input>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-map-marker"></i>
                    </div>
                    <input readonly style="width: 100%" class="branch form-control" />
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-check-circle"></i>
                    </div>
                    <input type="text" class="customer_favorite form-control" placeholder="Sở thích">
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Bếp
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" readonly class="kitchen_date form-control" placeholder="Thời hạn">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input readonly style="width: 100%" class="kitchen_time form-control" />
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Nhân viên vận chuyển
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-car"></i>
                    </div>
                    <input type="text" readonly readonly class="distance form-control" placeholder="Km">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-dollar"></i>
                    </div>
                    <input readonly type="text" class="ship_money form-control" placeholder="Phí vận chuyển">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" readonly class="ship_date form-control" placeholder="Thời hạn">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input readonly style="width: 100%" class="ship_time form-control" />
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Đặt món
            </div>
            <div class="clearfix"></div>
            <div class="detail-customer-info">
              <table class="table table-bordered">
                <thead>
                <tr>
                  <th style="width: 40%">Tên sản phẩm</th>
                  <th style="width: 20%">Số lượng</th>
                  <th style="width: 25%">Giá</th>
                </tr>
                </thead>
                <tbody class="list_product"></tr>
                </tbody>
              </table>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="customer-info kitchen-info">
            <div class="title-customer-info">
              Ghi chú
            </div>
            <div class="detail-customer-info">
              <textarea class="notes form-control " rows="3"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="pull-left" style="color: red; font-size: 20px; line-height: 34px;"><strong>Tổng tiền:</strong> <span class="total_money">0</span>đ</div>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Đóng</button>
        </div>
      </form>
    </div>
  </div>
</div>