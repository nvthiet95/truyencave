<div class="modal fade" id="set-shipper">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><strong>GẮN NVVC</strong></h4>
      </div>
      <div class="modal-body">
        <form action="/update-bill" method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="col-md-8">
            <select name="shipper" style="width: 100%" type="text" class="shipper form-control">
              <option value=""></option>
              @foreach($shippers as $v1)
                <option value="{{ $v1->id }}">{{ $v1->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-4">
            <button class="btn btn-primary form-control" name="id" id="bill_set_shipper_id">Gắn NVVC</button>
          </div>
        </form>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>