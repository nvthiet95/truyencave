<header class="main-header">
  <a href="/" class="logo">
    <span class="logo-mini"><b>Pz</b>M</span>
    <span class="logo-lg"><b>Pizza</b>Manager</span>
  </a>
  <nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="user user-menu {{ $_SERVER['REQUEST_URI'] === '/' ? 'active' : '' }}">
          <a href="/">
            Trang quản lý
          </a>
        </li>
        <li class="user user-menu {{ $_SERVER['REQUEST_URI'] === '/bep' ? 'active' : '' }}">
          <a href="/bep">
            Màn hình bếp
          </a>
        </li>
        <li class="user user-menu {{ $_SERVER['REQUEST_URI'] === '/tong-dai' ? 'active' : '' }}">
          <a href="/tong-dai">
            Màn hình tổng đài
          </a>
        </li>
        <li class="user user-menu {{ $_SERVER['REQUEST_URI'] === '/truy-thu' ? 'active' : '' }}">
          <a href="/truy-thu">
            Truy thu
          </a>
        </li>
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            @foreach(listBranch() as $v)
              @if($v->id == session()->get('branch_id'))
                <strong>Cửa hàng:</strong> {{ $v->name }}
              @endif
            @endforeach
          </a>
          <ul class="dropdown-menu">
            @foreach(listBranch() as $v)
              <a class="link-branch" style="{{ $v->id == session()->get('branch_id') ? 'background-color: #d2d6de' : '' }}" href="?branch_id={{ $v->id }}">{{ $v->name }}</a>
            @endforeach
          </ul>
        </li>
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="/AdminLTE/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ auth()->user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
              <p>
                {{ auth()->user()->name }} - Web Developer
                <small>Member since Nov. 2012</small>
              </p>
            </li>
            <!-- Menu Body -->
            <li class="user-body">
              <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div>
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                  {{ csrf_field() }}
                  <button type="submit" class="btn btn-default btn-flat">Đăng xuất</button>
                </form>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>