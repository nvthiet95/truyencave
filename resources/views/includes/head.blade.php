<meta charset="utf-8" />
<title>@yield('title')</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('favicon.ico')}}" />
<link href="{{ asset('/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/bower_components/Ionicons/css/ionicons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/dist/css/skins/skin-blue.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/AdminLTE/dist/css/style.css')}}" rel="stylesheet" type="text/css"/>
{{--<link href="http://kqxs.ga/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="http://kqxs.ga/AdminLTE/dist/css/style.css" rel="stylesheet" type="text/css"/>--}}
