@extends('layouts.default')
@section('title', 'Pizza Manager')
@section('content')
  <div class="content-wrapper">
    <section class="content container-fluid">
      <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#create-bill">
        Tạo đơn hàng
      </button>
      <div class="clearfix"></div>
      <br />
      <div class="box-body">
        <div class="row">
          <table class="table table-bordered">
            <thead><tr>
              <th style="width: 10px">#</th>
              <th>Tên khách hàng</th>
              <th>Số điện thoại</th>
              <th>Địa chỉ</th>
              <th>T/g bếp</th>
              <th>T/g giao</th>
              <th>Phí v/c</th>
              <th>Tổng tiền</th>
              <th>Người v/c</th>
              <th>Người tạo</th>
              <th>Hành động</th>
            </tr>
            </thead>
            <tbody class="list_product_show">
            @if(!empty($bills))
              @foreach($bills as $v)
                <tr data-detail="{{ json_encode($v) }}" style="background-color: {{ !empty($v->shipper) ? '#f39c12' : ($v->status == -1 ? '#ff9d91' : '') }}">
                  <td>{{ $v->id }}</td>
                  <td>{{ $v->customer_name }}</td>
                  <td>{{ $v->customer_phone }}</td>
                  <td>{{ $v->customer_address }} - {{ $v->street->name }}</td>
                  <td>{{ date("H:i", strtotime($v->kitchen_end)) }}</td>
                  <td>{{ date("H:i", strtotime($v->ship_end)) }}</td>
                  <td>{{ $v->ship_price }}</td>
                  <td>{{ $v->total_price }}</td>
                  <td>{{ $v->shipper->name or '' }}</td>
                  <td>{{ $v->creator->name }}</td>
                  <td>
                    @if($v->status != -1)
                      <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Hành động <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                          <li class="action-bill cancel-bill" data-id="{{ $v->id }}">Hủy đơn hàng</li>
                          <li class="action-bill set-shipper" data-id="{{ $v->id }}">Gắn NVVC</li>
                        </ul>
                      </div>
                    @endif
                  </td>
                </tr>
            @endforeach
            @endif
            </thead>
          </table>
        </div>
      </div>
      @include('components.tao-don-hang')
      @include('components.xem-lai-don-hang')
      @include('components.huy-don-hang')
      @include('components.gan-nvvc')
    </section>
  </div>
@stop
@section('page_scripts')
  <script src="{{ asset('/AdminLTE/dist/js/script.js') }}"></script>
@stop
