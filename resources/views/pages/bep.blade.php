@extends('layouts.default')
@section('title', 'Màn hình bếp')
@section('page_styles')
  <link href="{{ asset('/AdminLTE/dist/css/equal-height-columns.css')}}" rel="stylesheet" type="text/css"/>
  <meta http-equiv="refresh" content="60" />
@stop
@section('content')
  <div class="content-wrapper">
    <section class="content container-fluid">
      <div class="row row-eq-height">
        @foreach($bills as $k => $v)
          <div class="a-bill-screen" style="background-color: {{ $v->status == 2 ? '#00a65a' : ($v->status == 1 ? '#f39c12' : '') }}">
            <div class="text-center">
              <form action="/update-bill" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="status" value="1" />
                <button type="submit" class="btn-success btn" name="id" value="{{ $v->bill_number }}"><strong>{{ $v->bill_number }}</strong></button>
              </form>
            </div>
            <div class="text-center">
              <h3><strong>{{ str_replace('---', 'h', date("H---i", strtotime($v->kitchen_end))) }}</strong></h3>
            </div>
            <div class="text-center">
              <p>
              @foreach(json_decode($v->detail) as $v1)
                @if($v1->price > 0)
                  <h3><strong>{{ explode(' - ', $v1->name)[0] }} x {{ $v1->amount }}</strong></h3>
                  @endif
                  @endforeach
                  </p>
            </div>
            @if(!empty($v->notes))
              <div class="text-center">
                <p><strong style="color: red">{{ $v->notes }}</strong></p>
              </div>
            @endif
            <div class="text-center">
              <p>{{ str_replace('---', 'h', date("H---i", strtotime($v->ship_end))) }}</p>
            </div>
          </div>
          @if(($k+1) % 5 == 0)
      </div>
      <div class="row row-eq-height">
        @endif
        @endforeach
      </div>
    </section>
  </div>
@stop
@section('page_scripts')
  <script>
    $(document).ready(function () {

    });
  </script>
@stop