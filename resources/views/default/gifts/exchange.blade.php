@extends('layouts.default')
@section('page_styles')
<link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
   <div class="row-fluid">
    <div class="span12">
       <!-- BEGIN PAGE TITLE & BREADCRUMB-->
      <h3 class="page-title">{{ trans('lang.GIFT_TABLE') }}</h3>
      <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

            <i class="icon-angle-right"></i>
        </li>
        <li>{{ trans('lang.LIST_EXCHANGE') }}</li>


      </ul>
     <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE PORTLET-->
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-edit"></i>{{ trans('lang.LIST_EXCHANGE') }}</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="javascript:;" class="reload"></a>
               </div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-hover table-bordered">
                  <thead>
                     <tr>
                        <th>{{ trans('lang.GIFT_ID') }}</th>
                        <th>{{ trans('lang.TALBE_EXCHANGE_USER_NAME') }}</th>
                        <th>{{ trans('lang.TALBE_EXCHANGE_GIFT_NAME') }}</th>
                        <th>{{ trans('lang.TALBE_EXCHANGE_STATUS') }}</th>
                     </tr>
                  </thead>
                  <tbody>
                   @isset($exchange)
                   @foreach($exchange as $row)
                   <tr>
                      <td>{{ $row->id }}</td>
                      <td>{{ $row->user()->first()->name }}</td>
                      <td>{{ $row->gift()->first()->name }}</td>
                      <td>
                        <span data-id="{{ $row->id }}" onclick="gifts.updateExchange(this)" class="status-exchange label label-{{ $row->exchanged == 1 ? 'success' : 'danger' }}">
                          {{ $row->exchanged == 1 ? trans('lang.RECEIVED_GIFT') : trans('lang.NOT_RECEIVED_GIFT') }}
                        </span>
                      </td>
                   </tr>
                   @endforeach
                   @endisset
                  </tbody>
               </table>
            </div>
         </div>
         <!-- END EXAMPLE TABLE PORTLET-->
      </div>
   </div>
   <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/scripts/form-validation.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/js_modules/gifts.js')}}" type="text/javascript"></script>
<script>
  var textConfirmUpdateStatus = "{{ trans('lang.CONFIRM_UPDATE_STATUS_EXCHANGE') }}";
   jQuery(document).ready(function() {
      App.init();
   });
</script>
@stop
