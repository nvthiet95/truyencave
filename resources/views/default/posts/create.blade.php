@extends('layouts.default')
@section('page_styles')
    <link href="{{ asset($theme . '/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset($theme . '/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/jquery-tags-input/jquery.tagsinput.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset($theme . '/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet"
          type="text/css"/>
@stop
@section('content')
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title">{{ trans('lang.POST_CREATE') }}</h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>{{ trans('lang.POST_CREATE') }}</li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <div class="tabbable tabbable-custom boxless">
                    <div class="tab-pane active" id="tab_1">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i></div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form id="formPosts" class="form-horizontal" method="POST"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="alert alert-success hide">
                                        <button class="close" data-dismiss="alert"></button>
                                        {{ trans('lang.COMMON_10') }}
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_NAME')}} <span class="required">*</span>
                                        </label>
                                        <div class="controls">
                                            <input type="text" name="name" data-required="1" class="span6 m-wrap">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_SLUG')}}
                                        </label>
                                        <div class="controls">
                                            <input type="text" name="slug" class="span6 m-wrap">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_MENU')}} <span class="required">*</span>
                                        </label>
                                        <div class="controls">
                                            <select name="menu_id" data-required="1" class="span6 m-wrap"
                                                    data-placeholder="{{trans('lang.POST_23')}}" tabindex="1">
                                                <option value="">{{trans('lang.POST_SELECT_MENU')}}</option>
                                                @isset($menus)
                                                    @foreach($menus as $row)
                                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                                        @foreach($row->children as $v)
                                                            <option value="{{$v->id}}">__{{$v->name}}</option>
                                                        @endforeach
                                                    @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">{{trans('lang.POST_IMAGE')}}</label>
                                        <div class="controls">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm-thumbnail" data-input="input-thumbnail" data-preview="preview-thumbnail" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                </span>
                                                <input value="" id="input-thumbnail" class="form-control" type="text" name="image" readonly />
                                            </div>
                                            <img id="preview-thumbnail" class="preview-image" src="{{ $previewImage }}" />
                                            <div><i>{{ trans('lang.POST_IMAGE_NOTE') }}</i></div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_DESCRIPTION')}} <span class="required">*</span>
                                        </label>
                                        <div class="controls">
                                            <textarea class="span12 m-wrap" name="description" data-required="1"
                                                      rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_CONTENT')}} <span class="required">*</span>
                                        </label>
                                        <div class="controls">
                                            <textarea class="span12 m-wrap" name="content" id="content" rows="6"
                                                      data-required="1"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_LIKE')}}
                                        </label>
                                        <div class="controls">
                                            <input type="number" name="likes" class="span6 m-wrap">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_VIEW')}}
                                        </label>
                                        <div class="controls">
                                            <input type="number" name="views" class="span6 m-wrap">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('lang.POST_VOTE')}}
                                        </label>
                                        <div class="controls">
                                            <input type="number" name="votes" class="span6 m-wrap">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">{{trans('lang.POST_TRADE_MARK')}}</label>
                                        <div class="controls">
                                            <select name="trademark_id" class="span6 m-wrap"
                                                    data-placeholder="{{trans('lang.POST_23')}}" tabindex="1">
                                                <option value="">{{trans('lang.POST_SELECT_TRADE')}}</option>
                                                @isset($trades)
                                                    @foreach($trades as $row)
                                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                                    @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">{{trans('lang.POST_ACTIVE')}}</label>
                                        <div class="controls">
                                            <label class="radio">
                                                <div class="radio" id="uniform-undefined">
                       <span class="checked">
                         <input type="radio" name="active" value="1" checked="" style="opacity: 0;">
                       </span>
                                                </div>
                                                {{trans('lang.YES')}}
                                            </label>
                                            <label class="radio">
                                                <div class="radio" id="uniform-undefined">
                       <span class="">
                           <input type="radio" name="active" value="0" style="opacity: 0;">
                       </span>
                                                </div>
                                                {{trans('lang.NO')}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn blue">{{trans('lang.COMMON_3')}}</button>
                                        <button type="button" class="btn"
                                                onclick='posts.cancel()'>{{trans('lang.COMMON_4')}}</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
    <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/tinymce/tinymce.min.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/js_modules/posts.js')}}" type="text/javascript"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <script>
        var createPostSuccess = "{{ trans('post.CREATE_SUCCESS') }}";
        var defaultImage = "{{ $inputImage }}";
        var linkImages = "{{ env('LINK_IMAGES') }}";
        jQuery(document).ready(function () {
            App.init();
            posts.init();
            posts.validate('');
            var editor_config = {
                path_absolute : "/",
                selector: "textarea#content",
                height: 250,
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            tinymce.init(editor_config);
        });
    </script>
    <!-- END JAVASCRIPTS -->
@stop
