@extends('layouts.default')
@section('page_styles')
    <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .select2.select2-container.select2-container--default {
            margin-top: 0;
        }
    </style>
@stop
@section('content')
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">{{ trans('lang.SIDEBAR_POST_ALL') }}</h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

                        <i class="icon-angle-right"></i>
                    </li>
                    <li>{{ trans('lang.SIDEBAR_POST_ALL') }}</li>


                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-edit"></i>{{ trans('lang.SIDEBAR_POST_ALL') }}</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="btn-group">
                                    <a href="{{url('/posts/create')}}" class="btn pull-right green">
                                        {{ trans('lang.SIDEBAR_ADD') }} <i class="icon-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="span8">
                                <div class="pull-right">
                                    <form action="" method="GET">
                                        <input type="text" name="key" value="{{ request()->get('key') }}" class="m-wrap medium">
                                        <select data-placeholder="Select Category" id="inputCategory" name="inputCategory">
                                            <option value="">SELECT CATEGORY</option>
                                            @foreach($menus as $v)
                                                <option value="{{ $v->id }}" {{ request()->get('inputCategory') == $v->id ? 'selected' : '' }}>{{ $v->name }}</option>
                                            @endforeach
                                        </select>
                                        <button href="" class="btn pull-right green">{{ trans('lang.SEARCH') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body" id="listPost">
                        <table class="table table-striped table-hover table-bordered" id="tbCategories">
                            <thead>
                            <tr>
                                <th>{{ trans('lang.POST_ID') }}</th>
                                <th>{{ trans('lang.POST_NAME') }}</th>
                                <th>{{ trans('lang.POST_DESCRIPTION') }}</th>
                                <th>{{ trans('lang.POST_IMAGE') }}</th>
                                <th>{{ trans('lang.POST_MENU') }}</th>
                                <th>{{ trans('lang.POST_AUTHOR') }}</th>
                                <th>{{ trans('lang.POST_LIKE') }}</th>
                                <th>{{ trans('lang.POST_VIEW') }}</th>
                                <th>{{ trans('lang.POST_VOTE') }}</th>
                                <th>{{ trans('lang.POST_ACTIVE') }}</th>
                                <th>{{ trans('lang.EDIT') }}</th>
                                <th>{{ trans('lang.REMOVE') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($posts)
                                @foreach($posts as $v)
                                    <tr id='post_{{$v->id}}'>
                                        <td>{{$v->id}}</td>
                                        <td>{{$v->name}}</td>
                                        <td>{{str_limit($v->description, 50)}}</td>
                                        <td><img src="{{ $v->image }}" width="80" alt="" /></td>
                                        <td>{{$v->menu->name}}</td>
                                        <td>{{$v->user->name}}</td>
                                        <td>{{$v->likes}}</td>
                                        <td>{{$v->views}}</td>
                                        <td>{{$v->votes}}</td>
                                        <td>
                                            @if($v->active)
                                                <span class="label label-success">{{ trans('lang.STATUS_OK') }}</span>
                                            @else
                                                <span class="label label-danger">{{ trans('lang.STATUS_DRAFT') }}</span>
                                            @endif
                                        </td>
                                        <td><a class="edit"
                                               href="{{url('/posts/' . $v->id) }}">{{ trans('lang.EDIT') }}</a></td>
                                        <td><a class="delete" href="javascript:;"
                                               onclick="posts.delete({{ $v->id }}, this)"
                                               data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="dataTables_info" id="sample_editable_1_info"></div>
                            </div>
                            <div class="span6">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {{ $posts->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js')}}"></script>
    <script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/scripts/form-validation.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/js_modules/posts.js')}}" type="text/javascript"></script>
    <script>
        var confirmRemove = "<?php echo trans('post.CONFIRM_REMOVE') ?>";
        var deleteError = "<?php echo trans('post.ERROR') ?>";
        jQuery(document).ready(function () {
            App.init();
          $("#inputCategory").select2();
        });
    </script>
@stop
