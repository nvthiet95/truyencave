@extends('layouts.default')
@section('title', trans('lang.USER_TABLES'))
@section('page_styles')
    <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">{{ trans('lang.USER_TABLES') }}</h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

                        <i class="icon-angle-right"></i>
                    </li>
                    <li>{{ trans('lang.USER_TABLES') }}</li>


                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-edit"></i>{{ trans('lang.USER_TABLES') }}</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <div class="btn-group">
                                <a class="btn green" href="{{ url('/users/create') }}">
                                    {{ trans('lang.SIDEBAR_ADD') }} <i class="icon-plus"></i>
                                </a>
                            </div>
                        </div>
                        <table class="table table-striped table-hover table-bordered" id="tbCategories">
                            <thead>
                            <tr>
                                <th>{{ trans('lang.USER_ID') }}</th>
                                <th>{{ trans('lang.USER_NAME') }}</th>
                                <th>{{ trans('lang.USER_EMAIL') }}</th>
                                <th>{{ trans('lang.USER_AVATAR') }}</th>
                                <th>{{ trans('lang.USER_NICKNAME') }}</th>
                                <th>{{ trans('lang.USER_PHONE') }}</th>
                                <th>{{ trans('lang.USER_POINTS') }}</th>
                                <th>{{ trans('lang.EDIT') }}</th>
                                <th>{{ trans('lang.REMOVE') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($users))
                                @foreach($users as $row)
                                    <tr class="" id="user_{{ $row->id }}">
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->email }}</td>
                                        <td><img src="{{ $row->avatar }}" width="50" alt="" /></td>
                                        <td>{{ $row->display_name }}</td>
                                        <td>{{ $row->phone }}</td>
                                        <td>{{ $row->points }}</td>
                                        <td><a class="edit"
                                               href="{{url('/users/' . $row->id) }}">{{ trans('lang.EDIT') }}</a></td>
                                        <td><a class="delete" href="javascript:;"
                                               onclick="users.delete({{ $row->id }}, this)"
                                               data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="dataTables_info" id="sample_editable_1_info"></div>
                            </div>
                            <div class="span6">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/scripts/form-validation.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/js_modules/users.js')}}" type="text/javascript"></script>
    <script>
        var textConfirmRemove = "{{ trans('user.CONFIRM_REMOVE') }}";
        var textError = "{{ trans('user.ERROR') }}";
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop
