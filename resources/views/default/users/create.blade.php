@extends('layouts.default')
@section('title', trans('lang.USER_CREATE'))
@section('page_styles')
    <link href="{{ asset($theme . '/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/bootstrap-datepicker/css/datepicker.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset($theme . '/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/jquery-tags-input/jquery.tagsinput.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset($theme . '/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title">{{ trans('lang.USER_CREATE') }}</h3>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-edit"></i>{{ trans('lang.FORM_CREATE') }}</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <form method="POST" id="formUsers" class="form-horizontal" novalidate="novalidate"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="alert alert-success hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    {{ trans('lang.COMMON_10') }}
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_NAME') }}<span class="required">*</span>
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="name" data-required="1" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_EMAIL') }}<span class="required">*</span>
                                    </label>
                                    <div class="controls">
                                        <input type="email" name="email" data-required="1" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_PASSWORD') }}<span class="required">*</span>
                                    </label>
                                    <div class="controls">
                                        <input type="password" name="password" data-required="1" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_PASSWORD_CONFIRM') }}<span class="required">*</span>
                                    </label>
                                    <div class="controls">
                                        <input type="password" name="password_confirmation" data-required="1"
                                               class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_NICKNAME') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="display_name" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_CARDS') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="cards" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_PHONE') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="phone" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_BIRTHDAY') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="birthday" placeholder="yyyy-mm-dd"
                                               class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_JOB') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="job" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_CITY') }}
                                    </label>
                                    <div class="controls">
                                        <select name="city" id="city" class="span6 m-wrap">
                                            <option value="">-- Chọn địa điểm --</option>
                                            @isset ($cities)
                                                @foreach($cities as $v)
                                                    <option value="{{ $v->id }}">{{ $v->city }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_WEBSITE') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="website" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_DESCRIPTION') }}
                                    </label>
                                    <div class="controls">
                                        <input type="text" name="description" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        {{ trans('lang.USER_POINTS') }}
                                    </label>
                                    <div class="controls">
                                        <input type="number" name="points" class="span6 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">{{trans('lang.USER_AVATAR')}}</label>
                                    <div class="controls">
                                        <div class="input-group">
                                          <span class="input-group-btn">
                                            <a id="lfm-avatar" data-input="input-avatar" data-preview="preview-avatar" class="btn btn-primary">
                                                <i class="fa fa-picture-o"></i> Choose
                                            </a>
                                          </span>
                                          <input id="input-avatar" value="{{ $inputAvatar }}" class="form-control" type="text" name="avatar" readonly />
                                        </div>
                                        <img id="preview-avatar" class="preview-image" src="{{ $previewAvatar }}">
                                        <div><i>{{ trans('lang.USER_INSTRUCTION_AVATAR') }}</i></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">{{trans('lang.USER_COVER')}}</label>
                                    <div class="controls">
                                        <div class="input-group">
                                          <span class="input-group-btn">
                                            <a id="lfm-cover" data-input="input-cover" data-preview="preview-cover" class="btn btn-primary">
                                              <i class="fa fa-picture-o"></i> Choose
                                            </a>
                                          </span>
                                          <input id="input-cover" value="{{ $inputCover }}" class="form-control" type="text" name="cover" readonly />
                                        </div>
                                        <img id="preview-cover" class="preview-image" src="{{ $previewCover }}">
                                        <div><i>{{ trans('lang.USER_INSTRUCTION_COVER') }}</i></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"></label>
                                    <div class="controls">
                                        <label class="radio">
                                            <div class="radio" id="uniform-undefined">
                                                 <span class="checked">
                                                     <input type="radio" name="admin" value="0" checked=""
                                                            style="opacity: 0;"/>
                                                 </span>
                                            </div>
                                            {{ trans('lang.NO') }}
                                        </label>
                                        <label class="radio">
                                            <div class="radio" id="uniform-undefined">
                                                <span class="">
                                                    <input type="radio" name="admin" value="1" style="opacity: 0;"/>
                                                </span>
                                            </div>
                                            {{ trans('lang.YES') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn blue"><i
                                                class="icon-ok"></i> {{ trans('lang.COMMON_3') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/js_modules/users.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script>
        var textSuccess = "{{ trans('user.CREATE_SUCCESS') }}";
        var textError = "{{ trans('user.CREATE_ERROR') }}";
        var defaultAvatar = "{{ $inputAvatar }}";
        var defaultCover = "{{ $inputCover }}";
        var linkImages = "{{ env('LINK_IMAGES') }}";
        jQuery(document).ready(function () {
            App.init();
            users.init();
            users.validate();
        });
    </script>
@stop
