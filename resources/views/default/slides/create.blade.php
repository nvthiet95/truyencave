@extends('layouts.default')
@section('page_styles')
@stop
@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
   <div class="row-fluid">
    <div class="span12">
      <h3 class="page-title">{{ trans('slide.ADD_SLIDE') }}</h3>
    </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE PORTLET-->
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-edit"></i>{{ trans('lang.FORM_CREATE') }}</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="javascript:;" class="reload"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="clearfix">
                  <form method="POST" id="formSlides" class="form-horizontal" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="alert alert-success hide">
                        <button class="close" data-dismiss="alert"></button>
                        {{ trans('lang.COMMON_10') }}
                     </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('slide.SLIDE_TITLE') }}<span class="required">*</span>
                       </label>
                       <div class="controls">
                           <input type="text" name="title" data-required="1" class="span6 m-wrap" />
                       </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('slide.SLIDE_LINK') }}
                       </label>
                       <div class="controls">
                           <input type="text" name="link" class="span6 m-wrap" />
                       </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('slide.SLIDE_DESCRIPTION') }}
                       </label>
                       <div class="controls">
                           <input type="text" name="description" class="span6 m-wrap" />
                       </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">{{trans('lang.POST_IMAGE')}}</label>
                      <div class="controls">
                        <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm-thumbnail" data-input="input-thumbnail" data-preview="preview-thumbnail" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                </span>
                          <input value="" readonly id="input-thumbnail" class="form-control" type="text" name="image">
                        </div>
                        <img id="preview-thumbnail" class="preview-thumbnail">
                      </div>
                    </div>
                    <div class="form-actions">
                      <button type="submit" class="btn blue"><i class="icon-ok"></i> {{ trans('lang.COMMON_3') }}</button>
                    </div>
                   </form>
               </div>
            </div>
         </div>
         <!-- END EXAMPLE TABLE PORTLET-->
      </div>
   </div>
   <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script src="{{ asset($theme . '/js_modules/slides.js')}}" type="text/javascript"></script>
<script>
   jQuery(document).ready(function() {
      App.init();
      slides.validate();
      slides.init();
   });
</script>
@stop
