@extends('layouts.default')
@section('page_styles')
    <link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">{{ trans('notification.ALL_NOTI') }}</h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

                        <i class="icon-angle-right"></i>
                    </li>
                    <li>{{ trans('notification.ALL_NOTI') }}</li>


                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-edit"></i>{{ trans('notification.ALL_NOTI') }}</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="javascript:;" class="reload"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="btn-group">
                                    <a href="{{url('/notifications/create')}}" class="btn pull-right green">
                                        {{ trans('notification.ADD_NOTI') }} <i class="icon-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-striped table-hover table-bordered" id="tbCategories">
                            <thead>
                            <tr>
                                <th>{{ trans('notification.NOTI_ID') }}</th>
                                <th>{{ trans('notification.NOTI_DESCRIPTION') }}</th>
                                <th>{{ trans('notification.NOTI_USER_READED') }}</th>
                                <th>{{ trans('notification.NOTI_TIME') }}</th>
                                <th>{{ trans('notification.EDIT') }}</th>
                                <th>{{ trans('notification.REMOVE') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($notifications)
                                @foreach($notifications as $v)
                                    <tr id='notifications_{{$v->id}}'>
                                        <td>{{$v->id}}</td>
                                        <td>{{str_limit($v->description, 100)}}</td>
                                        <td>{{$v->readed==1 ? 'READED' : 'UNREAD'}}</td>
                                        <td>{{$v->created_at}}</td>
                                        <td><a class="edit"
                                               href="{{url('/notifications/' . $v->id) }}">{{ trans('lang.EDIT') }}</a></td>
                                        <td><a class="delete" href="javascript:;"
                                               onclick="notifications.delete({{ $v->id }}, this)"
                                               data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="dataTables_info" id="sample_editable_1_info"></div>
                            </div>
                            <div class="span6">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {{ $notifications->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js')}}"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/scripts/form-validation.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/js_modules/notifications.js')}}" type="text/javascript"></script>
    <script>
        var textConfirmRemove = "<?php echo trans('notification.CONFIRM_REMOVE') ?>";
        var textError = "<?php echo trans('notification.ERROR') ?>";
        jQuery(document).ready(function () {
            App.init();
        });
    </script>
@stop
