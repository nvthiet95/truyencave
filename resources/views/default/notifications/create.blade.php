@extends('layouts.default')
@section('page_styles')
    <link href="{{ asset($theme . '/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset($theme . '/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/jquery-tags-input/jquery.tagsinput.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset($theme . '/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset($theme . '/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet"
          type="text/css"/>
@stop
@section('content')
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title">{{ trans('notification.CREATE') }}</h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>{{ trans('notification.CREATE') }}</li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <div class="tabbable tabbable-custom boxless">
                    <div class="tab-pane active" id="tab_1">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-reorder"></i></div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                    <a href="javascript:;" class="reload"></a>
                                    <a href="javascript:;" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form id="formNotifications" class="form-horizontal" method="POST"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="alert alert-success hide">
                                        <button class="close" data-dismiss="alert"></button>
                                        {{ trans('lang.COMMON_10') }}
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('notification.NOTI_USER_RECIVE_NAME')}}
                                        </label>
                                        <div class="controls">
                                            <select class="js-data-example-ajax m-wrap span4" name="user_id"></select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            {{trans('notification.NOTI_DESCRIPTION')}} <span class="required">*</span>
                                        </label>
                                        <div class="controls">
                                            <textarea class="span6 m-wrap" name="description" data-required="1"
                                                      rows="4"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <button type="submit" class="btn blue">{{trans('lang.COMMON_3')}}</button>
                                        <button type="button" class="btn"
                                                onclick='notifications.cancel()'>{{trans('lang.COMMON_4')}}</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
    <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset($theme . '/js_modules/notifications.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        var textSuccess = "<?php echo trans('notification.CREATE_SUCCESS') ?>";
        var textError = "<?php echo trans('notification.CREATE_ERROR') ?>";
        jQuery(document).ready(function () {
            // initiate layout and plugins
            App.init();
            notifications.init();
            notifications.validate('');
          $(".js-data-example-ajax").select2({
            ajax: {
              url: "/users/find-user",
              dataType: 'json',
              data: function (params) {
                return {
                  key: params.term
                };
              },
              processResults: function (data, params) {
                return {
                  results: data
                };
              }
            },
            placeholder: 'Tìm kiếm người dùng',
            escapeMarkup: function (markup) { return markup; },
            templateResult: function (repo) {
              if (repo.loading) {
                return 'Đang tải dữ liệu...';
              }
              var markup = repo.name;
              return markup;
            },
            templateSelection: function (repo) {
              return repo.name || repo.text;
            }
          });
        });
    </script>
    <!-- END JAVASCRIPTS -->
@stop
