@extends('layouts.default')
@section('title', 'Danh sách game')
@section('page_styles')
  <link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
  <!-- BEGIN PAGE CONTAINER-->
  <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{ trans('game.LEFTBAR_ALL') }}</h3>
        <ul class="breadcrumb">
          <li>
            <i class="icon-home"></i>
            <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

            <i class="icon-angle-right"></i>
          </li>
          <li>{{ trans('game.LEFTBAR_ALL') }}</li>


        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
      <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption"><i class="icon-edit"></i>{{ trans('game.LEFTBAR_ALL') }}</div>
            <div class="tools">
              <a href="javascript:;" class="collapse"></a>
              <a href="javascript:;" class="reload"></a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="row-fluid">
              <div class="span4">
                <div class="btn-group">
                  <a href="{{url('/games/create')}}" class="btn pull-right green">
                    {{ trans('game.LEFTBAR_ADD') }} <i class="icon-plus"></i>
                  </a>
                </div>
              </div>
            </div>
            <table class="table table-striped table-hover table-bordered" id="tbCategories">
              <thead>
              <tr>
                <th>{{ trans('game.ID') }}</th>
                <th>{{ trans('game.CREATE_TITLE') }}</th>
                <th>{{ trans('game.CREATE_DESC') }}</th>
                <th>{{ trans('game.CREATE_FINISHED_AT') }}</th>
                <th>{{ trans('game.CREATE_POINT') }}</th>
                <th>{{ trans('game.CREATE_TASKS') }}</th>
                <th>{{ trans('game.GAME_STATUS') }}</th>
                <th>{{ trans('lang.EDIT') }}</th>
                <th>{{ trans('lang.REMOVE') }}</th>
              </tr>
              </thead>
              <tbody>
              @isset($games)
                @foreach($games as $v)
                  <tr id='game_{{$v->id}}'>
                    <td>{{$v->id}}</td>
                    <td>{{$v->title}}</td>
                    <td>{{$v->desc}}</td>
                    <td>{{$v->finished_at}}</td>
                    <td>{{$v->points}}</td>
                    <td>{{$v->tasks()->count()}} task</td>
                    <td>
                      @if($v->status == 1)
                        <span class="label label-success">Actived</span>
                      @else
                        <span class="label label-default">Not active</span>
                      @endif
                    </td>
                    <td><a class="edit"
                           href="{{url('/games/' . $v->id) }}">{{ trans('lang.EDIT') }}</a></td>
                    <td><a class="delete" href="javascript:;"
                           onclick="deleteGame({{ $v->id }}, this)"
                           data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            <div class="row-fluid">
              <div class="span6">
                <div class="dataTables_info" id="sample_editable_1_info"></div>
              </div>
              <div class="span6">
                <div class="dataTables_paginate paging_bootstrap pagination">
                  {{ $games->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <!-- END PAGE CONTENT -->
  </div>
  <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}"
          type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js')}}"></script>
  <script src="{{ asset($theme . '/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
  <script>
    jQuery(document).ready(function () {
      App.init();
    });
    function deleteGame(id, self) {
      swal({
        title: "Bạn có chắc muốn xóa game này không?",
        icon: "warning",
        buttons: ["Hủy bỏ",  {
          text: "Đồng ý",
          closeModal: false,
        }],
        dangerMode: true,
      }).then(function (result) {
        if(result) {
          $.ajax({
            url: '/games/' + id,
            type : "DELETE",
            dateType: "json",
            data : {id: id},
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success : function (data) {
              swal(data.message, "",  "success");
              $('#game_'+id).remove();
            },
            error: function(data) {
              swal(data.message, "", "error");
            }
          });
        }
      });
    }
  </script>
@stop
