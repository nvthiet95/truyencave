@extends('layouts.default')
@section('title', trans('game.CREATE_ADD'))
@section('page_styles')
  <link href="{{ asset($theme . '/plugins/bootstrap-datepicker/css/datepicker.css')}}" rel="stylesheet"
        type="text/css"/>
  <link href="{{ asset($theme . '/plugins/toastr/toastr.min.css')}}" rel="stylesheet"
        type="text/css"/>
  <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet"
        type="text/css"/>
  <link href="{{ asset($theme . '/plugins/ladda/ladda-themeless.min.css')}}" rel="stylesheet"
        type="text/css"/>
@stop
@section('content')
  <!-- BEGIN PAGE CONTAINER-->
  <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12">
        <h3 class="page-title">{{ trans('game.CREATE_ADD') }}</h3>
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
      <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption"><i class="icon-edit"></i>{{ trans('game.CREATE_FORM_CREATE') }}</div>
            <div class="tools">
              <a href="javascript:;" class="collapse"></a>
              <a href="javascript:;" class="reload"></a>
            </div>
          </div>
          <div class="portlet-body" ng-app="game" ng-controller="GameController">
            <div class="clearfix">
              <form method="POST" id="formSlides" class="form-horizontal" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="control-group <% game.title ? 'success' : (checkValue ? 'error' : '') %>">
                  <label class="control-label">
                    {{ trans('game.CREATE_TITLE') }}<span class="required">*</span>
                  </label>
                  <div class="controls">
                    <input ng-model="game.title" type="text" data-required="1" class="span6 m-wrap" autofocus/>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">
                    {{ trans('game.CREATE_DESC') }}
                  </label>
                  <div class="controls">
                    <input ng-model="game.desc" type="text" data-required="1" class="span6 m-wrap"/>
                  </div>
                </div>
                <div class="control-group hidden">
                  <label class="control-label">
                    {{ trans('game.CREATE_POINT') }}
                  </label>
                  <div class="controls">
                    <input ng-model="game.point" readonly type="text" data-required="1" class="span6 m-wrap"/>
                  </div>
                </div>
                <div class="control-group <% game.finished_at ? 'success' : (checkValue ? 'error' : '') %>">
                  <label class="control-label">
                    {{ trans('game.CREATE_FINISHED_AT') }}<span class="required">*</span>
                  </label>
                  <div class="controls">
                    <input type="text" ng-model="game.finished_at" name="finished_at" placeholder="yyyy-mm-dd"
                           class="span6 m-wrap"/>
                  </div>
                </div>
                <div class="control-group <% game.tasks && game.tasks.length > 0 ? 'success' : (checkValue ? 'error' : '') %>">
                  <label class="control-label">
                    {{ trans('game.CREATE_TASKS') }}
                  </label>
                  <div class="controls">
                    <div class="item-task-list" ng-if="game.tasks.length > 0">
                      <div ng-repeat='task in game.tasks'>
                        <div class="span7 m-wrap item-task">
                          <strong class="type-task-<% task.type %>"> (<% task.point %> point)</strong>:
                          <a href="/posts/<% task.object_id %>" target="_blank"><% task.object_name %></a>
                          <button class="btn red" ng-click="removeTask($index)">XÓA</button>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="item-task-list <% (checkValue ? 'error' : '') %>" ng-if="game.tasks.length == 0">
                      Chưa chọn tasks nào
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">
                    {{ trans('game.CREATE_SELECT_TASKS') }}
                  </label>
                  <div class="controls">
                    <select class="span2 m-wrap" ng-model="newTask.type">
                      <option value="">SELECT TYPE</option>
                      <option value="LIKE">{{ trans('task.like_name') }}</option>
                      <option value="SHARE">{{ trans('task.share_name') }}</option>
                      <option value="VIEW">{{ trans('task.view_name') }}</option>
                      <option value="CREATE_POST">{{ trans('task.create_post_name') }}</option>
                    </select>
                    <input type="number" ng-model="newTask.point" placeholder="Nhập point..."
                           data-required="1" class="span2 m-wrap"/>
                    <div style="display: <% newTask.type == 'LIKE' || newTask.type == 'VIEW' || newTask.type == 'SHARE' ? 'inline' : 'none' %>">
                      <select class="js-data-example-ajax m-wrap span4" id="selectTask" ng-model="newTask.object_id"></select>
                    </div>
                    <div style="display: <% newTask.type == 'CREATE_POST' ? 'inline' : 'none' %>">
                      <input type="number" ng-model="newTask.number" name="number" placeholder="Số lượng..."
                             class="span2 m-wrap"/>
                    </div>
                    <a ng-click="addTask()"
                            class="btn blue"><i class="icon-ok"></i> {{ trans('task.ADD') }}</a>
                    <br /><br />
                    <div class="<% classNoti %>"><% contentNoti %></div>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">
                    Active
                  </label>
                  <div class="controls">
                    <label class="radio">
                      <div class="radio" id="uniform-undefined">
                       <span class="">
                         <div class="radio" id="uniform-undefined"><span class="checked"><input type="radio" name="status" ng-model="game.status" value="1" checked="" style="opacity: 0;"></span></div>
                       </span>
                      </div>
                      Yes
                    </label>
                    <label class="radio">
                      <div class="radio" id="uniform-undefined">
                       <span class="">
                           <div class="radio" id="uniform-undefined"><span class=""><input type="radio" name="status" ng-model="game.status" value="0" style="opacity: 0;"></span></div>
                       </span>
                      </div>
                      No
                    </label>
                    <div><strong>Chú ý: </strong><i>Nếu chọn <strong>Yes</strong>, game đang hoạt động hiện tại sẽ bị dừng</i></div>
                  </div>
                </div>
                <div class="form-actions">
                  <button type="submit"
                          data-style="expand-right"
                          data-spinner-color="#fff"
                          ng-click="submitCreateGame()"
                          class="btn blue ladda-button submit-game">
                    <i class="icon-ok"></i> {{ trans('lang.COMMON_3') }}
                    <span class="ladda-spinner"></span></button>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <!-- END PAGE CONTENT -->
  </div></div>
  <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/toastr/toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/toastr/ui-toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/toastr/ui-toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/ladda/spin.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/ladda/ladda.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{ asset($theme . '/plugins/angular/angular.min.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/js_modules/game.js')}}" type="text/javascript"></script>
  <script>
    var gameInit = {
      title: '',
      desc: '',
      point: 0,
      finished_at: '',
      tasks: [],
      status: 0
    };
    jQuery(document).ready(function () {
      App.init();
    });
  </script>
@stop
