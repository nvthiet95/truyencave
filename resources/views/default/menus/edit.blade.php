@extends('layouts.default')
@section('page_styles')
<link href="{{ asset($theme . '/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/chosen-bootstrap/chosen/chosen.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/jquery-tags-input/jquery.tagsinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
   <div class="row-fluid">
    <div class="span12">
      <h3 class="page-title">{{ trans('lang.MENU_EDIT') }}</h3>
    </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE PORTLET-->
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-edit"></i>{{ trans('lang.MENU_EDIT') }}</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="javascript:;" class="reload"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="clearfix">
                  <form method="POST" id="formMenus" class="form-horizontal" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="alert alert-success hide">
                        <button class="close" data-dismiss="alert"></button>
                        {{ trans('lang.COMMON_10') }}
                     </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('lang.MENU_NAME') }}<span class="required">*</span>
                       </label>
                       <div class="controls">
                           <input type="text" name="name" data-required="1" value="{{ $menu->name }}" class="span6 m-wrap" />
                       </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('lang.MENU_SLUG') }}
                       </label>
                       <div class="controls">
                           <input type="text" name="slug" value="{{ $menu->slug }}" class="span6 m-wrap" />
                       </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('lang.MENU_TYPE') }}
                       </label>
                       <div class="controls">
                         <select name="type" id="type" class="span6 m-wrap">
                           <option value="REVIEW" @if($menu->type === 'REVIEW')  {!! 'selected' !!} @endif>REVIEW</option>
                           <option value="NEWS" @if($menu->type === 'NEWS')  {!! 'selected' !!} @endif>NEWS</option>
                           <option value="QUESTION" @if($menu->type === 'QUESTION')  {!! 'selected' !!} @endif>QUESTION</option>
                           <option value="TIPS" @if($menu->type === 'TIPS')  {!! 'selected' !!} @endif>TIPS</option>
                         </select>
                       </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('lang.MENU_PARENTS') }}
                       </label>
                       <div class="controls">
                         <select name="parents" id="parents" class="span6 m-wrap">
                           <option value="0">Uncategories</option>
                           @isset($menus)
                           @foreach ($menus as $v)
                           <option value="{{ $v->id }}" @if ($v->id === $menu->parents) {!! 'selected' !!} @endif>{{ $v->name }}</option>
                           @endforeach
                           @endisset
                         </select>
                       </div>
                    </div>
                    <div class="control-group">
                       <label class="control-label">
                         {{ trans('lang.MENU_DESCRIPTTION') }}
                       </label>
                       <div class="controls">
                         <textarea rows="5" name="description" id="description" class="span6 m-wrap"></textarea>
                       </div>
                    </div>
                    <div class="form-actions">
                      <button type="submit" class="btn blue"><i class="icon-ok"></i> {{ trans('lang.COMMON_3') }}</button>
                    </div>
                   </form>
               </div>
            </div>
         </div>
         <!-- END EXAMPLE TABLE PORTLET-->
      </div>
   </div>
   <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/js_modules/menus.js')}}" type="text/javascript"></script>
<script>
   jQuery(document).ready(function() {
      App.init();
      menus.validate({{ $menu->id }});
   });
</script>
@stop
