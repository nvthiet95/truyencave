@extends('layouts.default')
@section('page_styles')
<link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
   <div class="row-fluid">
    <div class="span12">
       <!-- BEGIN PAGE TITLE & BREADCRUMB-->
      <h3 class="page-title">{{ trans('lang.SIDEBAR_MENU_ALL') }}</h3>
      <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

            <i class="icon-angle-right"></i>
        </li>
        <li>{{ trans('lang.SIDEBAR_MENU_ALL') }}</li>


      </ul>
     <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE PORTLET-->
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-edit"></i>{{ trans('lang.SIDEBAR_MENU_ALL') }}</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="javascript:;" class="reload"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="clearfix">
                  <div class="btn-group">
                      <a class="btn green" href="{{ url('/menus/create') }}">
                        {{ trans('lang.SIDEBAR_ADD') }} <i class="icon-plus"></i>
                     </a>
                  </div>
               </div>
               <table class="table table-striped table-hover table-bordered" id="tbCategories">
                  <thead>
                     <tr>
                        <th>{{ trans('lang.MENU_NAME') }}</th>
                        <th>{{ trans('lang.MENU_SLUG') }}</th>
                        <th>{{ trans('lang.MENU_TYPE') }}</th>
                        <th>{{ trans('lang.EDIT') }}</th>
                        <th>{{ trans('lang.REMOVE') }}</th>
                     </tr>
                  </thead>
                  <tbody>
                   @isset ($menus)
                   @foreach($menus as $row)
                   <tr class="" id="menu_{{ $row->id }}">
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->slug }}</td>
                      <td>{{ $row->type }}</td>
                      <td><a class="edit" href="{{url('/menus/' . $row->id) }}">{{ trans('lang.EDIT') }}</a></td>
                      <td><a class="delete" href="javascript:;" onclick="menus.delete({{ $row->id }}, this)" data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                   </tr>
                     @foreach($row->children as $v)
                     <tr class="" id="menu_{{ $row->id }}">
                        <td>{{ '___ ' . $v->name }}</td>
                        <td>{{ $v->slug }}</td>
                        <td>{{ $v->type }}</td>
                        <td><a class="edit" href="{{url('/menus/' . $v->id) }}">{{ trans('lang.EDIT') }}</a></td>
                        <td><a class="delete" href="javascript:;" onclick="menus.delete({{ $v->id }}, this)" data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                     </tr>
                     @endforeach
                   @endforeach
                   @endif
                  </tbody>
               </table>
            </div>
         </div>
         <!-- END EXAMPLE TABLE PORTLET-->
      </div>
   </div>
   <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset($theme . '/plugins/jquery-validation/dist/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/scripts/form-validation.js')}}" type="text/javascript"></script>
<script src="{{ asset($theme . '/js_modules/menus.js')}}" type="text/javascript"></script>
<script>
  jQuery(document).ready(function () {
    App.init();
  });
</script>
@stop
