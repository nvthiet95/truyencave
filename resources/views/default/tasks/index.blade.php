@extends('layouts.default')
@section('title', trans('task.ALL'))
@section('page_styles')
  <link href="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset($theme . '/plugins/toastr/toastr.min.css')}}" rel="stylesheet"
        type="text/css"/>
  <link href="{{ asset($theme . '/plugins/select2/select2_metro.css')}}" rel="stylesheet"
        type="text/css"/>
  <link href="{{ asset($theme . '/plugins/ladda/ladda-themeless.min.css')}}" rel="stylesheet"
        type="text/css"/>
@stop
@section('content')
  <!-- BEGIN PAGE CONTAINER-->
  <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{ trans('game.LEFTBAR_ALL') }}</h3>
        <ul class="breadcrumb">
          <li>
            <i class="icon-home"></i>
            <a href="{{ url('/') }}">{{ trans('lang.SIDEBAR_DASHBOARD') }}</a>

            <i class="icon-angle-right"></i>
          </li>
          <li>{{ trans('task.ALL') }}</li>


        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
      <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption"><i class="icon-edit"></i>{{ trans('task.ADD') }}</div>
            <div class="tools">
              <a href="javascript:;" class="collapse"></a>
              <a href="javascript:;" class="reload"></a>
            </div>
          </div>
          <div class="portlet-body" ng-app="createGame" ng-controller="CreateController">
            <form method="POST" class="formCreateTask" class="form-horizontal" novalidate="novalidate">
              @if(!empty($games))
                <select class="span2 m-wrap" ng-model="task.game_id">
                  <option value="">SELECT GAME</option>
                  @foreach($games as $game)
                    <option value="{{ $game->id }}">{{ $game->title }}</option>
                  @endforeach
                </select>
              @endif
              <select class="span2 m-wrap" ng-model="task.type">
                <option value="">SELECT TYPE</option>
                <option value="LIKE">{{ trans('task.like_name') }}</option>
                <option value="SHARE">{{ trans('task.share_name') }}</option>
                <option value="VIEW">{{ trans('task.view_name') }}</option>
              </select>
              <input type="number" ng-model="task.point" placeholder="Nhập point..."
                     data-required="1" class="span2 m-wrap"/>
              <select class="js-data-example-ajax m-wrap span4" ng-model="task.object_id"></select>
                <button type="submit"
                        data-style="expand-right"
                        data-spinner-color="#fff"
                        ng-click="submitCreateGame()"
                        class="btn blue ladda-button submit-task">
                  <i class="icon-ok"></i> {{ trans('lang.COMMON_3') }}
                  <span class="ladda-spinner"></span></button>
            </form>
            <br />
            <div class="<% classNoti %>"><% contentNoti %></div>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET--><!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption"><i class="icon-edit"></i>{{ trans('task.ALL') }}</div>
            <div class="tools">
              <a href="javascript:;" class="collapse"></a>
              <a href="javascript:;" class="reload"></a>
            </div>
          </div>
          <div class="portlet-body" id="listTask">
            <table class="table table-striped table-hover table-bordered" id="tableTask">
              <thead>
              <tr>
                <th>{{ trans('task.ID') }}</th>
                <th>{{ trans('task.GAME_TITLE') }}</th>
                <th>{{ trans('task.TYPE') }}</th>
                <th>{{ trans('task.OBJECT') }}</th>
                <th>{{ trans('task.POINT') }}</th>
                <th>{{ trans('lang.REMOVE') }}</th>
              </tr>
              </thead>
              <tbody>
              @isset($tasks)
                @foreach($tasks as $v)
                  <tr id='task_{{$v->id}}'>
                    <td>{{$v->id}}</td>
                    <td><a href="/games/{{$v->game()->first()->id or ''}}" target="_blank">{{$v->game()->first()->title or "Error".$v->id}}</a></td>
                    <td>
                      <strong class="name-{{ strtolower($v->type) }}">{{ trans('task.'.strtolower($v->type).'_name') }}</strong>
                    <td><a href="/posts/{{$v->post()->first()->id or ''}}" target="_blank">{{$v->post()->first()->name or "Error: ".$v->id}}</a></td>
                    <td>{{$v->point}}</td>
                    <td><a class="delete" href="javascript:;"
                           onclick="deleteTask({{ $v->id }}, this)"
                           data-token="{{ csrf_token() }}">{{ trans('lang.REMOVE') }}</a></td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            <div class="row-fluid">
              <div class="span6">
                <div class="dataTables_info" id="sample_editable_1_info"></div>
              </div>
              <div class="span6">
                <div class="dataTables_paginate paging_bootstrap pagination">
                  {{ $tasks->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <!-- END PAGE CONTENT -->
  </div>
  <!-- END PAGE CONTAINER-->
@stop
@section('page_scripts')
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{ asset($theme . '/plugins/data-tables/jquery.dataTables.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/data-tables/DT_bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/angular/angular.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/toastr/toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/toastr/ui-toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/toastr/ui-toastr.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/ladda/spin.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/ladda/ladda.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/plugins/bootstrap-sweetalert/sweetalert.min.js')}}"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="{{ asset($theme . '/scripts/app.js')}}" type="text/javascript"></script>
  <script src="{{ asset($theme . '/js_modules/task.js')}}" type="text/javascript"></script>
  <script>
    jQuery(document).ready(function () {
      App.init();
    });
  </script>
@stop
