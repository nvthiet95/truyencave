$(document).ready(function () {
  $('.shipper').select2({
    placeholder: 'Chọn NVVC...'
  });
  $('.cancel-bill').click(function () {
    $('#cancel-bill').modal('show');
    $('#bill_cancel_id').val($(this).data('id'));
  });
  $('.set-shipper').click(function () {
    $('#set-shipper').modal('show');
    $('#bill_set_shipper_id').val($(this).data('id'));
  });

  $('#submit-cancel-bill').submit(function (e) {
    e.preventDefault();
    $('.submit-cancel-bill').button('loading');
    var data = {
      id: $('#bill_cancel_id').val(),
      reason_cancel: $('.reason-cancel-bill').val(),
    };
    $.ajax({
      url: '/update-bill',
      type: 'POST',
      data: data,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
        location.reload();
      }
    });
    console.log(data);
  });
  $('[name=streets]').select2({
    placeholder: "Địa chỉ"
  }).on('change', function () {
    $('.icon-branch').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajax({
      url: '/get-distance?street_id='+$('[name=streets]').val(),
      success: function (data) {
        if(data.length > 0) {
          $('[name=branch]').html('');
          for(var i=0; i<data.length; i++) {
            $('[name=branch]').append('<option value="'+data[i].branch.id+'_'+data[i].amount+'">'+data[i].branch.name+' ( '+data[i].amount+' km )</option>');
            if(i==0) {
              shipConfig(data[i].amount);
            }
          }
        }
        $('.icon-branch').html('<i class="fa fa-map-marker"></i>');
      },
      fail: function () {
        $('.icon-branch').html('<i class="fa fa-map-marker"></i>');
      }
    });
  });
  var list_product = [];
  $('#add_product').click(function () {
    if($('.product').val() !== '' && $('.amount').val() !== '') {
      var data = $('.product').select2('data');
      if(data) {
        var split = data[0].id.split('_');
        list_product.push({
          name: data[0].text,
          amount: Number($('.amount').val()),
          price: Number(split[1])
        });
        updateListProduct();
      }
    }
  });
  $(document).on('click', '.del_product', function () {
    list_product.splice($(this).data('index'),1);
    updateListProduct();
  });
  function updateTotalPrice() {
    var total = $('[name=ship_money]').val() !== '' ? Number($('[name=ship_money]').val()) : 0;
    for(var i=0; i<list_product.length; i++) {
      total += list_product[i].amount*list_product[i].price;
    }
    $('#total_money').html(total);
  }
  function updateListProduct() {
    $('.list_product').html('');
    for(var i=0; i<list_product.length; i++) {
      $('.list_product').append('<tr><td>'+list_product[i].name+'</td><td>'+list_product[i].amount+'</td><td>'+(list_product[i].amount*list_product[i].price)+'</td><td><button type="button" class="btn del_product btn-danger form-control" data-index="'+i+'">Xóa</button></td></tr>');
    }
    updateTotalPrice();
  }
  function shipConfig(km) {
    $('[name=distance]').val(km);
    if(km <= 4) $('[name=ship_money]').val('0');
    else {
      $('[name=ship_money]').val((Math.ceil(km-4)*10000));
    }
    updateTotalPrice();
  }
  $('[name=branch]').change(function () {
    var split = $('[name=branch]').val().split('_');
    if(split.length==2) {
      shipConfig(split[1]);
    }
  });
  $('[name=kitchen_date]').datepicker({
    autoclose: true,
    format: 'dd/mm/yyyy'
  });
  $('[name=kitchen_time]').select2({
    placeholder: "Thời gian bếp phải làm xong bánh"
  });
  $('.product').select2({
    placeholder: "Chọn món..."
  });
  $('[name=ship_date]').datepicker({
    autoclose: true,
    format: 'dd/mm/yyyy'
  })
  $('[name=ship_time]').select2({
    placeholder: "Thời gian bếp phải làm xong bánh"
  });
  $('#submit-create-bill').submit(function (e) {
    e.preventDefault();
    $('.submit-create-bill').button('loading');
    var split = $('[name=branch]').val().split('_');
    var data = {
      customer_phone: $('[name=customer_phone]').val(),
      customer_name: $('[name=customer_name]').val(),
      customer_address: $('[name=customer_address]').val(),
      streets: $('[name=streets]').val(),
      customer_favorite: $('[name=customer_favorite]').val(),
      branch: split[0],
      kitchen_date: $('[name=kitchen_date]').val(),
      kitchen_time: $('[name=kitchen_time]').val(),
      distance: $('[name=distance]').val(),
      ship_money: $('[name=ship_money]').val(),
      ship_date: $('[name=ship_date]').val(),
      ship_time: $('[name=ship_time]').val(),
      notes: $('[name=notes]').val(),
      products: list_product,
      distance: $('[name=distance]').val(),
      total_money: Number($('#total_money').html()),
    };
    $.ajax({
      url: '/create-bill',
      type: 'POST',
      data: data,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
        location.reload();
      }
    });
  });
  $(document).on('dblclick', '.list_product_show tr', function () {
    if($(this).hasClass('shipper-statis')) {
      $('.bill-shipper-'+$(this).data('id')).toggle();
    } else {
      $('#show-bill').modal('show');
      var data_detail = $(this).data('detail');
      $('#show-bill .customer_phone').val(data_detail.customer_phone);
      $('#show-bill .customer_name').val(data_detail.customer_name);
      $('#show-bill .customer_address').val(data_detail.customer_address);
      $('#show-bill .streets').val(data_detail.street.name);
      $('#show-bill .branch').val(data_detail.branch.name + ' ( ' + data_detail.distance + ' km )');
      $('#show-bill .customer_favorite').val(data_detail.customer_favorite);
      $('#show-bill .distance').val(data_detail.distance);
      $('#show-bill .notes').val(data_detail.notes);
      $('#show-bill .total_money').html(data_detail.total_price);
      $('#show-bill .ship_money').val(data_detail.ship_price);
      $('#show-bill .kitchen_date').val(formatDate(data_detail.kitchen_end));
      $('#show-bill .kitchen_time').val(formatTime(data_detail.kitchen_end));
      $('#show-bill .ship_date').val(formatDate(data_detail.ship_end));
      $('#show-bill .ship_time').val(formatTime(data_detail.ship_end));
      var list_product = JSON.parse(data_detail.detail);
      $('#show-bill .list_product').html('');
      for(var i=0; i<list_product.length; i++) {
        $('#show-bill .list_product').append('<tr><td>'+list_product[i].name+'</td><td>'+list_product[i].amount+'</td><td>'+(list_product[i].amount*list_product[i].price)+'</td></tr>');
      }
    }
  })
  $('[name=customer_phone]').blur(function () {
    if($('#create-bill [name=customer_phone]').val() !== '') {
      $.ajax({
        url: '/check-customer?customer_phone='+$('#create-bill [name=customer_phone]').val(),
        success: function (data) {
          if(data.id) {
            $('[name=customer_name]').val(data.customer_name);
            $('[name=customer_address]').val(data.customer_address);
            $('[name=customer_favorite]').val(data.customer_favorite);
            $('[name=streets]').html($('[name=streets]').html().replace('value="'+data.street_id+'"', 'value="'+data.street_id+'" selected')).trigger("change");
          }
        }
      });
    }
  })
})
function formatDate(dateOld) {
  const date = new Date(dateOld);
  const month = (date.getMonth()+1) < 10 ? '0'+(date.getMonth()+1) : (date.getMonth()+1);
  const year = date.getFullYear() < 10 ? '0'+date.getFullYear() : date.getFullYear();
  const day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
  return day+'/'+month+'/'+year;
}
function formatTime(dateOld) {
  const date = new Date(dateOld);
  const h = date.getHours() < 10 ? '0'+date.getHours() : date.getHours();
  const i = date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes();
  return h+':'+i;
}