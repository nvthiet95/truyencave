<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/truy-thu', 'HomeController@truyThu')->name('truyThu');

Route::get('/get-distance', 'ApiController@getDistance');
Route::post('/create-bill', 'ApiController@postCreateBill');
Route::get('/check-customer', 'ApiController@getCheckCustomer');
Route::get('/bep', 'ScreenController@bep');
Route::get('/tong-dai', 'ScreenController@tongDai');
Route::post('/update-bill', 'ApiController@postUpdateBill');